#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define GRIDSIZE 30



SDL_Color grid_background = {255, 255, 255, 255};                   //variables de couleur de la grille
SDL_Color grid_line_color = {0, 0, 0, 255};
SDL_Color grid_cursor_ghost_color = {220, 220, 220, 255};
SDL_Color grid_cursor_full_color = {0, 0, 0, 255};
int stableaeteaffiche = 0;
int worldType;

void end_sdl(char ok,                                               // fin normale : ok = 0 ; anormale ok = 1
             char const* msg,                                       // message à afficher
             SDL_Window* window,                                    // fenêtre à fermer
             SDL_Renderer* renderer) {                              // renderer à fermer
  char msg_formated[255];
  int l;

  if (!ok) {                                                        // Affichage de ce qui ne va pas
    strncpy(msg_formated, msg, 250);
    l = strlen(msg_formated);
    strcpy(msg_formated + l, " : %s\n");

    SDL_Log(msg_formated, SDL_GetError());
  }

  if (renderer != NULL) {                                           // Destruction si nécessaire du renderer
    SDL_DestroyRenderer(renderer);                                  // Attention : on suppose que les NULL sont maintenus !!
    renderer = NULL;
  }
  if (window != NULL)   {                                           // Destruction si nécessaire de la fenêtre
    SDL_DestroyWindow(window);                                      // Attention : on suppose que les NULL sont maintenus !!
    window= NULL;
  }

  SDL_Quit();

  if (!ok) {
    exit(EXIT_FAILURE);
  }
}

void initTab(int current_grid[GRIDSIZE][GRIDSIZE],                  // Initialisation des matrices à 0.
            int futur_grid[GRIDSIZE][GRIDSIZE]) {
  for(int i = 0; i < GRIDSIZE; i++) {
    for(int j = 0; j < GRIDSIZE; j++) {
      current_grid[i][j] = 0;
      futur_grid[i][j] = 0;
    }
  }
}

void initSDL() {
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
      SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Initialize SDL: %s",
                   SDL_GetError());
      exit(EXIT_FAILURE);
  }
}

void initCells(SDL_Rect gridRects[GRIDSIZE][GRIDSIZE]) {
  for(int j = 0; j < GRIDSIZE; j++){
    for(int i = 0; i < GRIDSIZE; i++) {
    gridRects[i][j].x = (GRIDSIZE - 1) / 2 * GRIDSIZE;
    gridRects[i][j].y = (GRIDSIZE - 1) / 2 * GRIDSIZE;
    gridRects[i][j].w = GRIDSIZE;
    gridRects[i][j].h = GRIDSIZE;
    }
  }
}

int neighbours(int current_grid[GRIDSIZE][GRIDSIZE], int i, int j){

  int nb_neighbours = 0;

      if(i == 0){
        if(j == 0){
          nb_neighbours=current_grid[i+1][j] +
                        current_grid[i][j+1] +
                        current_grid[i+1][j+1];
        }
        else if(j == GRIDSIZE-1){
          nb_neighbours=current_grid[i][j-1] +
                        current_grid[i+1][j-1] +
                        current_grid[i+1][j];
        }

        else{
          nb_neighbours=current_grid[i][j-1] +
                        current_grid[i+1][j-1] +
                        current_grid[i+1][j] +
                        current_grid[i][j+1] +
                        current_grid[i+1][j+1];
        }
      }

  else if(i == GRIDSIZE-1){
    if(j == 0){
      nb_neighbours=current_grid[i-1][j] +
                    current_grid[i-1][j+1] +
                    current_grid[i][j+1];
   }
    else if(j == GRIDSIZE-1){
      nb_neighbours=current_grid[i-1][j-1] +
                   current_grid[i][j-1] +
                   current_grid[i-1][j];
    }
   else{
     nb_neighbours=current_grid[i-1][j-1] +
                   current_grid[i][j-1] +
                   current_grid[i-1][j] +
                   current_grid[i-1][j+1] +
                   current_grid[i][j+1];
    }
  }
  else{
    if(j == 0){
      nb_neighbours=current_grid[i-1][j] +
                    current_grid[i+1][j] +
                    current_grid[i-1][j+1] +
                    current_grid[i][j+1] +
                    current_grid[i+1][j+1];
    }
    else if(j == GRIDSIZE-1){
      nb_neighbours=current_grid[i-1][j-1] +
                    current_grid[i][j-1] +
                    current_grid[i+1][j-1] +
                    current_grid[i-1][j] +
                    current_grid[i+1][j];
    }
    else{
      nb_neighbours=current_grid[i-1][j-1] +
                    current_grid[i][j-1] +
                    current_grid[i+1][j-1] +
                    current_grid[i-1][j] +
                    current_grid[i+1][j] +
                    current_grid[i-1][j+1] +
                    current_grid[i][j+1] +
                    current_grid[i+1][j+1];
    }
  }
  return nb_neighbours;
}


int neighbours_torus(int current_grid[GRIDSIZE][GRIDSIZE], int i, int j){

  int nb_neighbours = 0;

      if(i == 0){
        if(j == 0){
          nb_neighbours=current_grid[i+1][j] +
                        current_grid[i][j+1] +
                        current_grid[i+1][j+1]+
                        current_grid[i][GRIDSIZE-1]+
                        current_grid[i+1][GRIDSIZE-1]+
                        current_grid[GRIDSIZE-1][GRIDSIZE-1]+
                        current_grid[GRIDSIZE-1][j]+
                        current_grid[GRIDSIZE-1][j+1];
        }
        else if(j == GRIDSIZE-1){
          nb_neighbours=current_grid[i][j-1] +
                        current_grid[i+1][j-1] +
                        current_grid[i+1][j]+
                        current_grid[GRIDSIZE-1][j-1]+
                        current_grid[GRIDSIZE-1][j]+
                        current_grid[0][j]+
                        current_grid[0][j+1]+
                        current_grid[GRIDSIZE-1][0];
        }

        else{
          nb_neighbours=current_grid[i][j-1] +
                        current_grid[i+1][j-1] +
                        current_grid[i+1][j] +
                        current_grid[i][j+1] +
                        current_grid[i+1][j+1]+
                        current_grid[GRIDSIZE-1][j-1]+
                        current_grid[GRIDSIZE-1][j]+
                        current_grid[GRIDSIZE-1][j+1];
        }
      }

  else if(i == GRIDSIZE-1){
    if(j == 0){
      nb_neighbours=current_grid[i-1][j] +
                  current_grid[i-1][j+1] +
                  current_grid[i][j+1]+
                  current_grid[0][j]+
                  current_grid[0][j+1]+
                  current_grid[GRIDSIZE-1][j]+
                  current_grid[GRIDSIZE-1][j-1]+
                  current_grid[GRIDSIZE-1][0];
   }
    else if(j == GRIDSIZE-1){
      nb_neighbours=current_grid[i-1][j-1] +
                  current_grid[i][j-1] +
                  current_grid[i-1][j]+
                  current_grid[0][j]+
                  current_grid[0][j-1]+
                  current_grid[GRIDSIZE-1][j]+
                  current_grid[GRIDSIZE-1][j-1]+
                  current_grid[0][0];
    }
   else{
     nb_neighbours=current_grid[i-1][j-1] +
                  current_grid[i][j-1] +
                  current_grid[i-1][j] +
                  current_grid[i-1][j+1] +
                  current_grid[i][j+1]+
                  current_grid[0][j-1]+
                  current_grid[0][j]+
                  current_grid[0][j+1];;
    }
  }
  else{
    if(j == 0){
      nb_neighbours=current_grid[i-1][j] +
                    current_grid[i+1][j] +
                    current_grid[i-1][j+1] +
                    current_grid[i][j+1] +
                    current_grid[i+1][j+1]+
                    current_grid[i-1][GRIDSIZE-1] +
                    current_grid[i][GRIDSIZE-1] +
                    current_grid[i+1][GRIDSIZE-1];
    }
    else if(j == GRIDSIZE-1){
      nb_neighbours=current_grid[i-1][j-1] +
                    current_grid[i][j-1] +
                    current_grid[i+1][j-1] +
                    current_grid[i-1][j] +
                    current_grid[i+1][j]+
                    current_grid[i-1][0] +
                    current_grid[i][0] +
                    current_grid[i+1][0];
    }
    else{
      nb_neighbours=current_grid[i-1][j-1] +
                    current_grid[i][j-1] +
                    current_grid[i+1][j-1] +
                    current_grid[i-1][j] +
                    current_grid[i+1][j] +
                    current_grid[i-1][j+1] +
                    current_grid[i][j+1] +
                    current_grid[i+1][j+1];
    }
  }
  return nb_neighbours;
}

void updateTab(int current_grid[GRIDSIZE][GRIDSIZE],               // Modification des matrices.
              int futur_grid[GRIDSIZE][GRIDSIZE], int s[9],
              int n[9]){
  int i,j;
  int nb_neighbours = 0;
  for (i = 0; i < GRIDSIZE; i++){
    for (j = 0; j < GRIDSIZE; j++){
      if(worldType){
        nb_neighbours = neighbours_torus(current_grid, i, j);
      }
      else{
        nb_neighbours = neighbours(current_grid, i, j);
      }
      if (current_grid[i][j]){
        futur_grid[i][j] = s[nb_neighbours];
      }
      else{
        futur_grid[i][j]=n[nb_neighbours];
      }
    }
  }
  int stable = 1;
  for (i = 0; i < GRIDSIZE; i++){
    for (j = 0; j < GRIDSIZE; j++){
      if(current_grid[i][j] != futur_grid[i][j]){
        stable = 0;
      }
        current_grid[i][j]=futur_grid[i][j];
    }
  }
  if(stable && !stableaeteaffiche){
    printf("L'etat est stable\n");
    stableaeteaffiche = 1;
  }
}

void colorAllCells(SDL_Renderer *renderer,                          // Coloriage des cases vivantes.
                  int current_grid[GRIDSIZE][GRIDSIZE],
                  SDL_Rect gridCells[GRIDSIZE][GRIDSIZE]){
  int i,j;

  for (i = 0; i < GRIDSIZE; i++){
    for (j = 0; j < GRIDSIZE; j++){
      if(current_grid[i][j]){
        SDL_SetRenderDrawColor(renderer, grid_cursor_full_color.r,
                               grid_cursor_full_color.g,
                               grid_cursor_full_color.b,
                               grid_cursor_full_color.a);
        gridCells[i][j].x = i * GRIDSIZE;
        gridCells[i][j].y = j * GRIDSIZE;

        SDL_RenderFillRect(renderer, &gridCells[i][j]);
      }


    }
  }
}

void saveConfig(char* fileName, int grid[GRIDSIZE][GRIDSIZE]) {
  FILE* file;
  file = fopen(fileName, "w");

  if(file == NULL) {
    printf("File error\n");
    exit(1);
  }

  for(int i = 0; i < GRIDSIZE; i++) {
    for(int j = 0; j < GRIDSIZE; j++){
      fprintf(file, "%d", grid[i][j]);
    }
    fprintf(file, "\n");
  }
  fclose(file);
}

int main(){

    int survie[9] = {0, 0, 1, 1, 0, 0, 0, 0, 0};
    int naissance[9] = {0, 0, 0, 1, 0, 0, 0, 0, 0};
    int current_grid[GRIDSIZE][GRIDSIZE];
    int futur_grid[GRIDSIZE][GRIDSIZE];

    int window_width = (GRIDSIZE * GRIDSIZE) + 1;
    int window_height = (GRIDSIZE * GRIDSIZE) + 1;

    SDL_Rect grid_cursor_ghost = {(GRIDSIZE - 1) / 2 * GRIDSIZE,
                                  (GRIDSIZE - 1) / 2 * GRIDSIZE,
                                  GRIDSIZE,
                                  GRIDSIZE};

    SDL_Rect gridCells[GRIDSIZE][GRIDSIZE];


    printf( "Enter world Mode :");
    scanf("%d",  &worldType);
    if(worldType){
      printf( "\nYou entered Torus mode\n");
    }
    else{
      printf("\nYou entered bordered mode\n");
    }


    initTab(current_grid,futur_grid);
    initCells(gridCells);
    initSDL();

    SDL_Window *window;
    SDL_Renderer *renderer;
    if (SDL_CreateWindowAndRenderer(window_width, window_height, 0, &window,
                                    &renderer) < 0) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
                     "Create window and renderer: %s", SDL_GetError());
        return EXIT_FAILURE;
    }

    SDL_SetWindowTitle(window, "Game Of Life");

    SDL_bool quit = SDL_FALSE;
    SDL_bool mouse_active = SDL_FALSE;
    SDL_bool mouse_hover = SDL_FALSE;
    SDL_bool evolution = SDL_FALSE;
    int delay = 200;

    while (!quit) {
        SDL_Event event;
        if(!evolution){
          if(stableaeteaffiche){
            stableaeteaffiche = 0;
          }
          while (SDL_PollEvent(&event)) { // gestion clavier souris
            switch (event.type) {
              case SDL_KEYDOWN:
                switch(event.key.keysym.sym){
                  case SDLK_SPACE:
                    evolution = SDL_TRUE;
                    break;
                }
              break;
            case SDL_MOUSEBUTTONDOWN:
                if(current_grid[event.motion.x / GRIDSIZE][event.motion.y / GRIDSIZE]){
                  current_grid[event.motion.x / GRIDSIZE][event.motion.y / GRIDSIZE] = 0;
                }
                else{
                  current_grid[event.motion.x / GRIDSIZE][event.motion.y / GRIDSIZE] = 1;
                }
                break;
            case SDL_MOUSEMOTION:
                grid_cursor_ghost.x = (event.motion.x / GRIDSIZE) * GRIDSIZE;
                grid_cursor_ghost.y = (event.motion.y / GRIDSIZE) * GRIDSIZE;
                if (!mouse_active)
                    mouse_active = SDL_TRUE;
                break;
            case SDL_WINDOWEVENT:
                if (event.window.event == SDL_WINDOWEVENT_ENTER && !mouse_hover)
                    mouse_hover = SDL_TRUE;
                else if (event.window.event == SDL_WINDOWEVENT_LEAVE && mouse_hover)
                    mouse_hover = SDL_FALSE;
                break;
            case SDL_QUIT:
                quit = SDL_TRUE;
                break;
            }
        }

        // Draw grid background.
        SDL_SetRenderDrawColor(renderer, grid_background.r, grid_background.g,
                               grid_background.b, grid_background.a);
        SDL_RenderClear(renderer);

        // Draw grid lines.
        SDL_SetRenderDrawColor(renderer, grid_line_color.r, grid_line_color.g,
                               grid_line_color.b, grid_line_color.a);

        for (int x = 0; x < 1 + GRIDSIZE * GRIDSIZE;
             x += GRIDSIZE) {
            SDL_RenderDrawLine(renderer, x, 0, x, window_height);
        }

        for (int y = 0; y < 1 + GRIDSIZE * GRIDSIZE;
             y += GRIDSIZE) {
            SDL_RenderDrawLine(renderer, 0, y, window_width, y);
        }

        // Draw grid ghost cursor.
        if (mouse_active && mouse_hover) {
            SDL_SetRenderDrawColor(renderer, grid_cursor_ghost_color.r,
                                   grid_cursor_ghost_color.g,
                                   grid_cursor_ghost_color.b,
                                   grid_cursor_ghost_color.a);
            SDL_RenderFillRect(renderer, &grid_cursor_ghost);
        }

        // Draw grid cursor.
        colorAllCells(renderer, current_grid, gridCells);

        SDL_RenderPresent(renderer);

        SDL_Delay(50);
        }
        else{
          while (SDL_PollEvent(&event)) { // gestion clavier souris
            switch (event.type) {
              case SDL_KEYDOWN:
                switch(event.key.keysym.sym){
                  case SDLK_SPACE:
                    evolution = SDL_FALSE;
                    break;
                  case SDLK_UP:
                    if(delay >= 80){
                      delay -= 30;
                    }
                    else{
                      delay = 50;
                    }
                    break;
                  case SDLK_DOWN:
                      delay += 30;
                      break;
                }
              break;
              case SDL_WINDOWEVENT:
                  if (event.window.event == SDL_WINDOWEVENT_ENTER && !mouse_hover)
                      mouse_hover = SDL_TRUE;
                  else if (event.window.event == SDL_WINDOWEVENT_LEAVE && mouse_hover)
                      mouse_hover = SDL_FALSE;
                  break;
              case SDL_QUIT:
                  quit = SDL_TRUE;
                  break;
            }
        }

        // Draw grid background.
        SDL_SetRenderDrawColor(renderer, grid_background.r, grid_background.g,
                               grid_background.b, grid_background.a);
        SDL_RenderClear(renderer);

        // Draw grid lines.
        SDL_SetRenderDrawColor(renderer, grid_line_color.r, grid_line_color.g,
                               grid_line_color.b, grid_line_color.a);

        for (int x = 0; x < 1 + GRIDSIZE * GRIDSIZE;
             x += GRIDSIZE) {
            SDL_RenderDrawLine(renderer, x, 0, x, window_height);
        }

        for (int y = 0; y < 1 + GRIDSIZE * GRIDSIZE;
             y += GRIDSIZE) {
            SDL_RenderDrawLine(renderer, 0, y, window_width, y);
        }

        // Draw grid ghost cursor.
        if (mouse_active && mouse_hover) {
            SDL_SetRenderDrawColor(renderer, grid_cursor_ghost_color.r,
                                   grid_cursor_ghost_color.g,
                                   grid_cursor_ghost_color.b,
                                   grid_cursor_ghost_color.a);
            SDL_RenderFillRect(renderer, &grid_cursor_ghost);
        }

        // Draw grid cursor.
        updateTab(current_grid,futur_grid,survie, naissance);
        colorAllCells(renderer, current_grid, gridCells);

        //SDL_RenderFillRect(renderer, &grid_cursor);

        SDL_RenderPresent(renderer);
        SDL_Delay(delay);
        }

    }
    saveConfig("test.txt", current_grid);

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return EXIT_SUCCESS;
}
