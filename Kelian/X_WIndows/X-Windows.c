#include <SDL2/SDL.h>
#include <stdio.h>
#include <math.h>

#define PI 3.14159265359


/*********************************************************************************************************************/  
/*                                                      Main                                                         */
/*********************************************************************************************************************/

int main(int argc, char **argv) {
  (void)argc;
  (void)argv;

/*============================================| Window Creation |====================================================*/
  SDL_Window* WindowTab[80];

  if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());
    exit(EXIT_FAILURE);
  }


  for ( int i = 0 ; i <= 79 ; i++)
  {
    WindowTab[i] =  SDL_CreateWindow("SDL WINDOWS X",850,300,200, 200,SDL_WINDOW_RESIZABLE);
    if (WindowTab[i] == NULL) {
      SDL_Log("Error : SDL window X creation - %s\n", 
      SDL_GetError());
      SDL_Quit();      
      exit(EXIT_FAILURE);
    }
    SDL_Delay(5);
  } 


  
  int x;
  int y;
  int t = 0;
  for ( int i = 0 ; i <= 79 ; i++)
  {
    x = 850 + 700*cos( 40*t/(2*PI) );
    y = 300 + 700*sin(2* 40*t/(2*PI) )/2;
    SDL_SetWindowPosition(WindowTab[i], (int) x, (int) y);
    t++;
    SDL_Delay(50);  
  }
  

  SDL_Delay(1000); 

  int x0;
  int y0;

  for ( int i = 0 ; i <= 500 ; i++)
  {
    for ( int j = 0 ; j <= 19 ; j++)
    {
      SDL_GetWindowPosition(WindowTab[j], &x0, &y0);
      if (x0 >= 850)
      {
        SDL_SetWindowPosition(WindowTab[j], x0 - 2, y0);
      }
    }
    for ( int j = 58 ; j <= 79 ; j++)
    {
      SDL_GetWindowPosition(WindowTab[j], &x0, &y0);
      if (x0 >= 850)
      {
        SDL_SetWindowPosition(WindowTab[j], x0 - 2, y0);
      }
    }
    
    for ( int j = 20 ; j <= 57 ; j++)
    {
      SDL_GetWindowPosition(WindowTab[j], &x0, &y0);
      if (x0 <= 850)
      {
        SDL_SetWindowPosition(WindowTab[j], x0 + 2, y0);
      }
    }

    SDL_Delay(25);  
  }


  for ( int i = 0 ; i <= 200 ; i++)
  {
    for ( int j = 0 ; j <= 79 ; j++)
    {
      SDL_GetWindowPosition(WindowTab[j], &x0, &y0);
      if (y0 <= 300)
      {
        SDL_SetWindowPosition(WindowTab[j], x0, y0  + 2 );
      }
      else
      {
        SDL_SetWindowPosition(WindowTab[j], x0, y0  - 2 );
      } 
    }
    
    SDL_Delay(25);  
  }

  SDL_Delay(1000);

  for ( int i = 79 ; i >= 0 ; i--)
  {
    SDL_DestroyWindow(WindowTab[i]);
    SDL_Delay(100);    
  }

  SDL_Delay(4000);  

  SDL_Quit();

  return 0;
}