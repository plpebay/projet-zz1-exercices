#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <math.h>
#include <stdio.h>
#include <string.h>



void end_sdl(char ok,                                               // fin normale : ok = 0 ; anormale ok = 1
             char const* msg,                                       // message à afficher
             SDL_Window* window,                                    // fenêtre à fermer
             SDL_Renderer* renderer) {                              // renderer à fermer
  char msg_formated[255];                                                         
  int l;                                                                          

  if (!ok) {                                                        // Affichage de ce qui ne va pas
    strncpy(msg_formated, msg, 250);                                              
    l = strlen(msg_formated);                                                     
    strcpy(msg_formated + l, " : %s\n");                                          

    SDL_Log(msg_formated, SDL_GetError());                                        
  }                                                                               

  if (renderer != NULL) {                                           // Destruction si nécessaire du renderer
    SDL_DestroyRenderer(renderer);                                  // Attention : on suppose que les NULL sont maintenus !!
    renderer = NULL;
  }
  if (window != NULL)   {                                           // Destruction si nécessaire de la fenêtre
    SDL_DestroyWindow(window);                                      // Attention : on suppose que les NULL sont maintenus !!
    window= NULL;
  }

  SDL_Quit();                                                                     

  if (!ok) {                                                        // On quitte si cela ne va pas                
    exit(EXIT_FAILURE);                                                           
  }                                                                               
}                                                                                 



//suprimer si pas besoin au final
void DisplayTexture(SDL_Texture *my_texture, SDL_Window *window,
                         SDL_Renderer *renderer) {
    SDL_Rect 
            source = {0},                         // Rectangle définissant la zone de la texture à récupérer
            window_dimensions = {0},              // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
            destination = {0};                    // Rectangle définissant où la zone_source doit être déposée dans le renderer

    SDL_GetWindowSize(
        window, &window_dimensions.w,
        &window_dimensions.h);                    // Récupération des dimensions de la fenêtre
    SDL_QueryTexture(my_texture, NULL, NULL,
                    &source.w, &source.h);       // Récupération des dimensions de l'image
    
    destination.h  = window_dimensions.h ;              // On fixe les dimensions de l'affichage à  celles de la fenêtre
    destination.w  = window_dimensions.w*2 ;

    /* On veut afficher la texture de façon à ce que l'image occupe la totalité de la fenêtre */

    SDL_RenderCopy(renderer, my_texture,
                    &source,
                    &destination);                 // Création de l'élément à afficher
}





int main(int argc, char** argv) {
  (void)argc;
  (void)argv;

  SDL_Window* Window = NULL;
  SDL_Renderer* renderer = NULL;
  SDL_Texture* texture = NULL;
  SDL_Texture* character = NULL;

  SDL_DisplayMode screen;

/*********************************************************************************************************************/  
/*                         Initialisation de la SDL  + gestion de l'échec possible                                   */
    if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", Window, renderer);

    SDL_GetCurrentDisplayMode(0, &screen);
    printf("Résolution écran\n\tw : %d\n\th : %d\n",
            screen.w, screen.h);

/* Création de la fenêtre */
    Window = SDL_CreateWindow("SDL Render Draw",
                                SDL_WINDOWPOS_CENTERED,
                                SDL_WINDOWPOS_CENTERED, screen.w * 0.60,
                                screen.h * 0.60,
                                SDL_WINDOW_OPENGL);
    if (Window == NULL) end_sdl(0, "ERROR WINDOW CREATION", Window, renderer);

/* Création du renderer */
    renderer = SDL_CreateRenderer(Window, -1,
                                    SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", Window, renderer);

/* Création de la texture */
/*background*/
    char* image_file  = "backgroundcave.png";

    texture = IMG_LoadTexture(renderer,image_file);
    if (texture == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", Window, renderer);
/*character*/
    character = IMG_LoadTexture(renderer,"RUN.png");
    if (character == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", Window, renderer);



/*********************************************************************************************************************/
/*                                     On dessine dans le renderer                                                   */
/*********************************************************************************************************************/

    SDL_bool quit = SDL_FALSE;
    SDL_Event event;                          


    SDL_Rect 
        source = {0},                             // Rectangle définissant la zone de la texture à récupérer
        window_dimensions = {0},                  // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
        destination = {0},
        Csource = {0},                    // Rectangle définissant la zone totale de la planche
        Cdestination = {0},               // Rectangle définissant où la zone_source doit être déposée dans le renderer
        Cstate = {0};                     // Rectangle de la vignette en cours dans la planche  

    SDL_GetWindowSize(Window, &window_dimensions.w, &window_dimensions.h);
    SDL_QueryTexture(texture, NULL, NULL, &source.w, &source.h);     

    float zoom = 1;

    int nb_it = 200;
    
    destination.w = window_dimensions.w * zoom*2;
    destination.h = window_dimensions.h * zoom;
    destination.y = (window_dimensions.h - destination.h) / 2; // On centre en hauteur

    float WD = window_dimensions.w - destination.w;  // longueur du déplacement à effectuer


    /***********************************/
    
    SDL_QueryTexture(character,           // Récupération des dimensions de l'image
                    NULL, NULL,
                    &Csource.w, &Csource.h);

    /* Mais pourquoi prendre la totalité de l'image, on peut n'en afficher qu'un morceau, et changer de morceau :-) */

    int nb_images = 24;                     // Il y a 8 vignette dans la ligne de l'image qui nous intéresse
    float Czoom = 0.5;                        // zoom, car ces images sont un peu petites
    int offset_x = 13 + Csource.w / nb_images,   // La largeur d'une vignette de l'image, marche car la planche est bien réglée
        offset_y = Csource.h;           // La hauteur d'une vignette de l'image, marche car la planche est bien réglée


    Cstate.x = 0 ;                          // La première vignette est en début de ligne
    //Cstate.y = 3 * offset_y;                // On s'intéresse à la 4ème ligne, le bonhomme qui court
    Cstate.w = offset_x;                    // Largeur de la vignette
    Cstate.h = offset_y;                    // Hauteur de la vignette

    Cdestination.w = offset_x * Czoom;       // Largeur du sprite à l'écran
    Cdestination.h = offset_y * Czoom;       // Hauteur du sprite à l'écran

    Cdestination.y = (window_dimensions.h - Cdestination.h); // La course se fait en milieu d'écran (en vertical)


    int i = 0;
    int j = 0;



    

    while (!quit){                           // Voilà la boucle des évènements 

        while (SDL_PollEvent(&event)&& !quit){                 // si la file d'évènements n'est pas vide : défiler l'élément en tête
                                                    // de file dans 'event'
            switch(event.type){                       // En fonction de la valeur du type de cet évènement
            case SDL_QUIT :                           // Un évènement simple, on a cliqué sur la x de la fenêtre
                quit = SDL_TRUE;                 // Il est temps d'arrêter le programme
                break;

            default:                                  // L'évènement défilé ne nous intéresse pas
                break;
            }
        }
        

        if ( i == nb_it)
        {
            i = 0;
        }

        if ( Cdestination.x == 1100)
        {
            j = 0;
        }

        //printf("%d\n", Cdestination.x);

        destination.x = WD * 0.5*i/nb_it;            // hauteur en fonction du numéro d'image

        Cdestination.x = j;                   // Position en x pour l'affichage du sprite
        Cstate.x += offset_x;                 // On passe à la vignette suivante dans l'image
        Cstate.x %= Csource.w;                 // La vignette qui suit celle de fin de ligne est
                                            // celle de début de ligne
        

        SDL_RenderClear(renderer);                    // Effacer l'image précédente

        //SDL_SetTextureAlphaMod(texture,(1.0-1.0*i/nb_it)*255);      // L'opacité va passer de 255 à 0 au fil de l'animation
        SDL_RenderCopy(renderer, texture, &source, &destination);   // Préparation de l'affichage

        SDL_RenderCopy(renderer, character, // Préparation de l'affichage
                    &Cstate,
                    &Cdestination);  

        SDL_RenderPresent(renderer);                  // Affichage de la nouvelle image
        SDL_Delay(41);

        i++;
        j+= 10;
    } 

    SDL_DestroyTexture(texture);
    IMG_Quit();
    /* on referme proprement la SDL */
    end_sdl(1, "Normal ending", Window, renderer);
    return EXIT_SUCCESS;
}
