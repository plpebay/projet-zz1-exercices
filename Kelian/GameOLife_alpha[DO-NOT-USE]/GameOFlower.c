#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

/************************************/
/*      Exercice Dessin Rendu       */
/************************************/

/*********************************************************************************************************************/  
/*                                              Variables entrées                                                    */
int WindowH = 800;    //window height
int WindowW = 800;    //window weight

int GridSizeL = 40; //case per line
int GridSizeC = 40; //case per column

int WorldState = 0; // 0 is limited & 1 is torus

int survival[9] = {0,0,1,1,0,0,0,0,0}; //based on living neighbour number
int born[9] = {0,0,0,1,0,0,0,0,0};  //based on living neighbour number



void end_sdl(char ok,                                               // fin normale : ok = 0 ; anormale ok = 1
             char const* msg,                                       // message à afficher
             SDL_Window* window,                                    // fenêtre à fermer
             SDL_Renderer* renderer) {                              // renderer à fermer
  char msg_formated[255];                                                         
  int l;                                                                          

  if (!ok) {                                                        // Affichage de ce qui ne va pas
    strncpy(msg_formated, msg, 250);                                              
    l = strlen(msg_formated);                                                     
    strcpy(msg_formated + l, " : %s\n");                                          

    SDL_Log(msg_formated, SDL_GetError());                                        
  }                                                                               

  if (renderer != NULL) {                                           // Destruction si nécessaire du renderer
    SDL_DestroyRenderer(renderer);                                  // Attention : on suppose que les NULL sont maintenus !!
    renderer = NULL;
  }
  if (window != NULL)   {                                           // Destruction si nécessaire de la fenêtre
    SDL_DestroyWindow(window);                                      // Attention : on suppose que les NULL sont maintenus !!
    window= NULL;
  }

  SDL_Quit();                                                                     

  if (!ok) {                                                        // On quitte si cela ne va pas                
    exit(EXIT_FAILURE);                                                           
  }                                                                               
}                                                                                 


/*********************************************************************************************************************/  
/*                                              Gestion graphique                                                    */

/*Grid Creation*/
void InsideGrid(SDL_Renderer* renderer,int h, int w, int sizeL, int sizeC, int matrix[sizeL][sizeC])
{
    int caseH;  //case height
    int caseW;  //case weight

    caseH = (int) ( h / sizeL );
    caseW = (int) ( w / sizeC );

    SDL_Rect Case[sizeL][sizeC];

    int x = 0; //coordinate case
    int y = 0; //coordinate case

    int color;

    for ( int i = 0 ; i <= sizeL-1 ; i++)
    {
        for ( int j = 0 ; j <= sizeC-1 ; j++)
        {
            if(matrix[i][j] == 1 )
            {
                SDL_SetRenderDrawColor(renderer,0, 0, 0,255);
                Case[i][j].x = x+i*caseW;
                Case[i][j].y = y+j*caseH;
                Case[i][j].w = caseW;
                Case[i][j].h = caseH;
                SDL_RenderFillRect(renderer, &Case[i][j]);
            }
            else
            {
                color = 255 - i*(255 /sizeL);

                SDL_SetRenderDrawColor(renderer,color, 255, 255,255);
                Case[i][j].x = x+i*caseW;
                Case[i][j].y = y+j*caseH;
                Case[i][j].w = caseW;
                Case[i][j].h = caseH;
                SDL_RenderFillRect(renderer, &Case[i][j]);
            }            
        }
    }
}

void CreateGrid(SDL_Renderer* renderer,int h, int w, int sizeL, int sizeC)
{
    int lineNum;
    int columnNum;
    lineNum = sizeL - 1;
    columnNum = sizeC - 1;

    SDL_SetRenderDrawColor(renderer, 200, 200, 200, 255);  

    int xcolumn;
    int yline;

    for ( int i = 1 ; i <= lineNum ; i++)
    {
        xcolumn  = (int) ( w / lineNum ) * i;
        SDL_RenderDrawLine(renderer, xcolumn, 0, xcolumn, h);
    }

    for ( int i = 1 ; i <= columnNum ; i++)
    {
        yline  = (int) ( h / columnNum ) * i;
        SDL_RenderDrawLine(renderer, 0, yline, w, yline);
    } 

}

/*Mouse monitoring*/
void HightLighMouse(SDL_Renderer* renderer, int x, int y, int h, int w, int sizeL, int sizeC)
{

    SDL_Rect Case;

    int xpos = 0;
    int ypos =  0;
    
    if ( ( y > 0  && y < h) && ( x > 0 && x < w) )
    {
        int i = (int)(w/sizeL);
        while( i <= w)
        {
            if( x > ( i - (int)(w/sizeL) ) && x < i )
            {   
                int j = (int)(h/sizeC);
                while( j <= h)
                {
                    if( y > ( j - (int)(h/sizeC) ) && y < j )
                    {
                        xpos = i - (int)(w/sizeL);
                        ypos =  j - (int)(h/sizeC);

                        SDL_SetRenderDrawColor(renderer,170, 170, 170,25);
                        Case.x = xpos;
                        Case.y = ypos;
                        Case.w = (int) w/sizeL;
                        Case.h = (int) h/sizeC;
                        SDL_RenderFillRect(renderer, &Case);
                    }
                    j+= (int)(h/sizeC);
                }
            }
            i+= (int)(w/sizeL);
        } 
    }
}


void FillGrid(int x, int y, int h, int w, int sizeL, int sizeC, int matrix[sizeL][sizeC])
{
    int xpos = 0;
    int ypos =  0;

    int xi;
    int yi;
    
    if ( ( y > 0  && y < h) && ( x > 0 && x < w) )
    {
        int i = (int)(w/sizeL);
        while( i <= w)
        {
            if( x > ( i - (int)(w/sizeL) ) && x < i )
            {   
                int j = (int)(h/sizeC);
                while( j <= h)
                {
                    if( y > ( j - (int)(h/sizeC) ) && y < j )
                    {
                        xpos = i - (int)(w/sizeL);
                        ypos = j - (int)(h/sizeC);

                        xi = (int) (xpos * sizeL / w);
                        yi = (int) (ypos * sizeC / h);

                        if ( matrix[xi][yi] == 0)
                        {
                            matrix[xi][yi] = 1; 
                        }
                        else
                        {
                            matrix[xi][yi] = 0; 
                        }

                    }
                    j+= (int)(h/sizeC);
                }
            }
            i+= (int)(w/sizeL);
        }
    }
}




/*********************************************************************************************************************/  
/*                                              Fonctionnement jeu                                                   */

int XX = 2;
int YY = 0;

int NeighbourState(int x, int y, int matrix[x][y], int i, int j, int world)
{   
    int nb_neighbours = 0;

    if(i == 0)
    {
        if(j == 0)
        {
          nb_neighbours=matrix[i+1][j] +
                        matrix[i][j+1] +
                        matrix[i+1][j+1];
        }
        else if(j == y-1)
        {
          nb_neighbours=matrix[i][j-1] +
                        matrix[i+1][j-1] +
                        matrix[i+1][j];
        }
        else
        {
          nb_neighbours=matrix[i][j-1] +
                        matrix[i+1][j-1] +
                        matrix[i+1][j] +
                        matrix[i][j+1] +
                        matrix[i+1][j+1];
        }
    }
    else if(i == x-1)
    {
        if(j == 0)
        {
            nb_neighbours=matrix[i-1][j] +
                        matrix[i-1][j+1] +
                        matrix[i][j+1];
        }
        else if(j == y-1)
        {
            nb_neighbours=matrix[i-1][j-1] +
                    matrix[i][j-1] +
                    matrix[i-1][j];
        }
        else
        {
            nb_neighbours=matrix[i-1][j-1] +
                    matrix[i][j-1] +
                    matrix[i-1][j] +
                    matrix[i-1][j+1] +
                    matrix[i][j+1];                        
        }
    }
    else
    {
        if(j == 0)
        {
            nb_neighbours=matrix[i-1][j] +
                        matrix[i+1][j] +
                        matrix[i-1][j+1] +
                        matrix[i][j+1] +
                        matrix[i+1][j+1];
        }
        else if(j == y-1)
        {
            nb_neighbours=matrix[i-1][j-1] +
                        matrix[i][j-1] +
                        matrix[i+1][j-1] +
                        matrix[i-1][j] +
                        matrix[i+1][j];
        }
        else
        {
            nb_neighbours=matrix[i-1][j-1] +
                        matrix[i][j-1] +
                        matrix[i+1][j-1] +
                        matrix[i-1][j] +
                        matrix[i+1][j] +
                        matrix[i-1][j+1] +
                        matrix[i][j+1] +
                        matrix[i+1][j+1];
        }
    }

  return nb_neighbours;


}

void ChangeMatrix(int x, int y, int matrix[x][y], int matrixCh[x][y], int xpos, int ypos, int living)
{


}

/*
void Itteration()
{

}
*/


int main(int argc, char** argv) {
  (void)argc;
  (void)argv;

  SDL_Window* Window = NULL;
  SDL_Renderer* renderer = NULL;

  //SDL_DisplayMode screen;



  /*********************************************************************************************************************/  
  /*                         Initialisation de la SDL  + gestion de l'échec possible                                   */
  if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", Window, renderer);

    /*
  SDL_GetCurrentDisplayMode(0, &screen);
  printf("Résolution écran\n\tw : %d\n\th : %d\n",
         screen.w, screen.h);
    */

  /* Création de la fenêtre */
  Window = SDL_CreateWindow("SDL Render Draw",
                            SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED, WindowW,
                            WindowH,
                            SDL_WINDOW_OPENGL);
  if (Window == NULL) end_sdl(0, "ERROR WINDOW CREATION", Window, renderer);

  /* Création du renderer */
  renderer = SDL_CreateRenderer(Window, -1,
                                SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", Window, renderer);

  /* Grid complete creation */
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);

    SDL_GetWindowSize(Window, &WindowW, &WindowH);
    printf("Résolution fenêtre\n\tw : %d\n\th : %d\n", WindowW, WindowH);






  /*********************************************************************************************************************/
  /*                                     On dessine dans le renderer                                                   */
  /*********************************************************************************************************************/
  /*             Cette partie pourrait avantageusement être remplacée par la boucle évènementielle                     */ 

    SDL_bool
        program_on = SDL_TRUE,               // Booléen pour dire que le programme doit continuer
        paused = SDL_FALSE;                             // Booléen pour dire que le programme est en pause
    SDL_Event event;                              // c'est le type IMPORTANT !!

    /*Mouse coordinate*/
    int Xmouse;
    int Ymouse;

    int alive = 0;

    /*Filling Matrix*/
    int MatrixG[GridSizeL][GridSizeC]; 
    int MatrixG1[GridSizeL][GridSizeC];
    int MatrixG2[GridSizeL][GridSizeC];

    for ( int i = 0 ; i <= GridSizeL-1 ; i++)
    {
        for ( int j = 0 ; j <= GridSizeC-1 ; j++)
        {
            MatrixG[i][j] = 0;
            MatrixG1[i][j] = 0;
            MatrixG2[i][j] = 0;
        }

    }

    int i =0;

    while (program_on){                           // Voilà la boucle des évènements 

        SDL_GetMouseState(&Xmouse, &Ymouse);

        if (SDL_PollEvent(&event)){                 // si la file d'évènements n'est pas vide : défiler l'élément en tête
                                                    // de file dans 'event'
            switch(event.type){                       // En fonction de la valeur du type de cet évènement
            case SDL_QUIT :                           // Un évènement simple, on a cliqué sur la x de la fenêtre
                program_on = SDL_FALSE;                 // Il est temps d'arrêter le programme
                break;

            /*Keyboard inputs*/
            case SDL_KEYDOWN:                             // Le type de event est : une touche appuyée
                                                            // comme la valeur du type est SDL_Keydown, dans la partie 'union' de
                                                            // l'event, plusieurs champs deviennent pertinents   
                switch (event.key.keysym.sym) {             // la touche appuyée est ...
                case SDLK_SPACE:                            // ou 'SPC'
                    paused = !paused;                         // basculement pause/unpause
                    printf("PAUSE!\n");
                    break;
                case SDLK_ESCAPE:                           // 'ESCAPE'  
                case SDLK_q:                                // ou 'q'
                    program_on = 0;                           // 'escape' ou 'q', d'autres façons de quitter le programme                                     
                    break;
                default:                                    // Une touche appuyée qu'on ne traite pas
                    break;
                }
                break;

            /*Mouse inputs*/
            case SDL_MOUSEBUTTONDOWN:  
                if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT) )
                {
                    FillGrid(Xmouse, Ymouse, WindowH, WindowW, GridSizeL, GridSizeC, MatrixG);
                }
                break;

            default:                                  // L'évènement défilé ne nous intéresse pas
            break;
            }
        }

        if (paused) {                                  // Si on n'est pas en pause
            
            /*Saving Matrix*/
            for ( int i = 0 ; i <= GridSizeL-1 ; i++)
            {
                for ( int j = 0 ; j <= GridSizeC-1 ; j++)
                {
                    MatrixG2[i][j] = MatrixG1[i][j];
                    MatrixG1[i][j] = MatrixG[i][j];
                }
            }

            for ( int i = 0 ; i <= GridSizeL-1 ; i++)
            {
                for ( int j = YY ; j <= YY/*GridSizeC-1*/ ; j++)
                {
                    alive = NeighbourState(GridSizeL, GridSizeC, MatrixG, XX, YY, WorldState);
                    printf("cellule:%d,%d\n\tvivants: %d\n", i, YY, alive);
                }
            }
            

            //ChangeMatrix(GridSizeL, GridSizeC, MatrixG, MatrixG1, i, j, live);

            //Itteration();
            printf("%d\n",i);
            i++;
            SDL_Delay(50);
        }


        InsideGrid(renderer, WindowH, WindowW, GridSizeL, GridSizeC, MatrixG);

        HightLighMouse(renderer, Xmouse, Ymouse, WindowH, WindowW, GridSizeL, GridSizeC);

        CreateGrid(renderer, WindowH, WindowW, GridSizeL, GridSizeC);

        // Affichages et calculs souvent ici
        SDL_RenderPresent(renderer);
        //draw(renderer);               // appel de la fonction qui crée l'image  
        // SDL_Delay(5);
        //SDL_RenderPresent(renderer);                         // affichage
        
    } 


  /* on referme proprement la SDL */
  end_sdl(1, "Normal ending", Window, renderer);
  return EXIT_SUCCESS;
}
