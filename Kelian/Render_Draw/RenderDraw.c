#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

/*********************************************************************************************************************/  
/*                                                 End Manager                                                       */

void end_sdl(char ok,                                               // fin normale : ok = 0 ; anormale ok = 1
             char const* msg,                                       // message à afficher
             SDL_Window* window,                                    // fenêtre à fermer
             SDL_Renderer* renderer) {                              // renderer à fermer
  char msg_formated[255];                                                         
  int l;                                                                          

  if (!ok) {                                                        // Affichage de ce qui ne va pas
    strncpy(msg_formated, msg, 250);                                              
    l = strlen(msg_formated);                                                     
    strcpy(msg_formated + l, " : %s\n");                                          

    SDL_Log(msg_formated, SDL_GetError());                                        
  }                                                                               

  if (renderer != NULL) {                                           // Destruction si nécessaire du renderer
    SDL_DestroyRenderer(renderer);                                  // Attention : on suppose que les NULL sont maintenus !!
    renderer = NULL;
  }
  if (window != NULL)   {                                           // Destruction si nécessaire de la fenêtre
    SDL_DestroyWindow(window);                                      // Attention : on suppose que les NULL sont maintenus !!
    window= NULL;
  }

  SDL_Quit();                                                                     

  if (!ok) {                                                        // On quitte si cela ne va pas                
    exit(EXIT_FAILURE);                                                           
  }                                                                               
}  


/*********************************************************************************************************************/  
/*                                               Display Manager                                                     */

void Snake (SDL_Renderer* renderer, int w, int h, int xsl, int ysl, int xel, int yel, int xS[20] , int yS[20]
            , int x[], int y[], int hr[], int wr[], int r[], int v[], int b[], int a[])
{

  SDL_Rect Back;

  SDL_SetRenderDrawColor(renderer,255, 255, 255,255);
  Back.x = 0;
  Back.y = 0;
  Back.w = w;
  Back.h = h;
  SDL_RenderFillRect(renderer, &Back);

  SDL_Rect rectangle[20];                                                             

  for ( int i = 0 ; i <= 19 ; i++)
  {
      SDL_SetRenderDrawColor(renderer,r[i], v[i], b[i],a[i]);
      rectangle[i].x = x[i];                                                  // x haut gauche du rectangle
      rectangle[i].y = y[i];                                                  // y haut gauche du rectangle
      rectangle[i].w = wr[i];                                                // sa largeur (w = width)
      rectangle[i].h = hr[i];                                                // sa hauteur (h = height)
      SDL_RenderFillRect(renderer, &rectangle[i]);  
  }


  SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);                   
  SDL_RenderDrawLine(renderer,                                      
                     xsl, ysl,                                          // x,y du point de la première extrémité
                     xel, yel);                                     // x,y seconde extrémité
  
  SDL_Rect Snake[20];
  
  for ( int i = 0 ; i <= 19 ; i++)
  {
      SDL_SetRenderDrawColor(renderer,0, 0, 0,255);
      Snake[i].x = xS[i];                                                 // x haut gauche du rectangle
      Snake[i].y = yS[i];                                                // y haut gauche du rectangle
      Snake[i].w = 10;                                                // sa largeur (w = width)
      Snake[i].h = 10;                                                // sa hauteur (h = height)
      SDL_RenderFillRect(renderer, &Snake[i]);
  }
  


}



/*********************************************************************************************************************/  
/*                                                      Main                                                         */
/*********************************************************************************************************************/

int main(int argc, char** argv) {
  (void)argc;
  (void)argv;

  SDL_Window* Window = NULL;
  SDL_Renderer* renderer = NULL;

  SDL_DisplayMode screen;

/*********************************************************************************************************************/  
/*                                  SDL Initialisation + Possible Failure Manager                                    */

  if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", Window, renderer);

  SDL_GetCurrentDisplayMode(0, &screen);
  printf("Résolution écran\n\tw : %d\n\th : %d\n",
         screen.w, screen.h);

  int Ww = 1400;
  int Wh = 800;

/*============================================| Window Creation |====================================================*/
  Window = SDL_CreateWindow("SDL Render Draw",
                            SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED, Ww,
                            Wh,
                            SDL_WINDOW_OPENGL);
  if (Window == NULL) end_sdl(0, "ERROR WINDOW CREATION", Window, renderer);

/*============================================| Renderer Creation |===================================================*/
  renderer = SDL_CreateRenderer(Window, -1,
                                SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", Window, renderer);


/*********************************************************************************************************************/  
/*                                                FInal Display                                                      */

    SDL_bool program_on = SDL_TRUE;
    SDL_Event event;
    
/*===========================================| Rectangles Creation |=================================================*/
  
    int Xrand[20];
    int Yrand[20];
    int Hrand[20];
    int Lrand[20];
    int Rrand[20];
    int Vrand[20];
    int Brand[20];
    int Arand[20];
  
/*==============================================| Snake Creation |===================================================*/

    int XSnake[20];
    int YSnake[20];

    XSnake[0] = 200;
    YSnake[0] = 200;

    int FalSIN[20] = {0,0,1,2,3,4,4,3,2,1,0,-1,-2,-3,-4,-4,-3,-2,-1,0};                                     

    for ( int i = 1 ; i <= 19 ; i++)
    {
        XSnake[i] = XSnake[i-1] + 10;
        YSnake[i] = YSnake[i-1];
    }

    int SigneS = 1;
    int signeSx[20] ={1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1} ;

/*===============================================| Lines Creation |==================================================*/

    int inc = 0;
    int signex = 1;
    int signey = 1;

    int Xcenter = 2*screen.w/6;
    int Ycenter = 2*screen.h/6;

    int xdrift = (rand() %10);
    int ydrift = (rand() %10);

    while (program_on){

      if (SDL_PollEvent(&event)){
          switch(event.type){
            case SDL_QUIT :
              program_on = SDL_FALSE;
              break;
            default:
              break;
          }
      }

/*===========================================| Rectangles Generation |===============================================*/

      for ( int i = 0 ; i <= 19 ; i++)
      {
          Xrand[i] = (rand() % 1238);
          Yrand[i] = (rand() % 614);
          Hrand[i] = (rand() % (100 - 5 + 1)) + 5;
          Lrand[i] = (rand() % (100 - 5 + 1)) + 5;
          Rrand[i] = rand() % 255;
          Vrand[i] = rand() % 255;
          Brand[i] = rand() % 255;
          Arand[i] = (rand() % (255 - 100 + 1)) + 100;
      }

/*==============================================| Lines Generation |=================================================*/

      int XSL = Xcenter + 200*cos(inc);
      int YSL = Ycenter + 200*sin(inc);
      int XEL = Xcenter + 200*cos(M_PI + inc);
      int YEL = Ycenter + 200*sin(M_PI + inc);
      
      Xcenter += signex * xdrift;
      Ycenter += signey * ydrift;
      
      /*boing*/
      if (Xcenter <= 0 || Xcenter >= Ww)
      {
        signex = -signex;
        xdrift = (rand() %10);
      }
      if ( Ycenter <= 0 || Ycenter >= Wh )
      {
        signey = -signey;
        ydrift = (rand() %10);
      }


/*==============================================| Snake Generation |==================================================*/

      for ( int i = 0 ; i <= 19 ; i++ )
      {
        if (XSnake[i] <= 0 || XSnake[i] >= Ww)
        {
          signeSx[i] = -signeSx[i];
        }
        XSnake[i] = XSnake[i] + signeSx[i] * 2; 
      }

      if ( SigneS == 1)
      {
        SigneS = 2;
        for ( int i = 0 ; i <= 19 ; i++)
        {
            YSnake[i] = YSnake[i] -  FalSIN[i];
        }
      }
      else if (SigneS == 2)
      {
        SigneS = -1;
        for ( int i = 0 ; i <= 19 ; i++)
        {
            YSnake[i] = YSnake[i] -  FalSIN[i];
        }
      }
      else if (SigneS == -1)
      {
        SigneS = 0;
        for ( int i = 0 ; i <= 19 ; i++)
        {
            YSnake[i] = YSnake[i] +  FalSIN[i];
        }
      } 
      else
      {
        SigneS = 1;
        for ( int i = 0 ; i <= 19 ; i++)
        {
            YSnake[i] = YSnake[i] +  FalSIN[i];
        }
      }

/*==================================================| Display |======================================================*/

      SDL_RenderClear(renderer);
      Snake(renderer, Ww, Wh, XSL, YSL, XEL, YEL, XSnake, YSnake, Xrand, Yrand, Hrand, Lrand, Rrand, Vrand, Brand, Arand);
      SDL_Delay(25);
      SDL_RenderPresent(renderer);

      inc ++;

    } 


  /* SDL Close */
  end_sdl(1, "Normal ending", Window, renderer);
  return EXIT_SUCCESS;
}
