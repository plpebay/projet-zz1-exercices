#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>

/************************************/
/*  exemple de création de fenêtres */
/************************************/

int main(int argc, char **argv) {
  (void)argc;
  (void)argv;

  SDL_Window *windowTab[200];
  int width = 100, height = 100;

  if (SDL_Init(SDL_INIT_VIDEO) == -1) {
    fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError());
    return EXIT_FAILURE;
  }

  int k = 0;
  for(int i = 0; i < 10; i++) {
    windowTab[k] = SDL_CreateWindow("fentrelambda", i * 100, 0,
              width, height,
              SDL_WINDOW_RESIZABLE);
    if (windowTab[k] == NULL) {
      SDL_Log("Error : SDL window creation - %s\n", SDL_GetError());
      SDL_Quit();
      exit(EXIT_FAILURE);
    }
    k++;
    windowTab[k] = SDL_CreateWindow("fentrelambda", 0, i * 100,
              width, height,
              SDL_WINDOW_RESIZABLE);
    if (windowTab[k] == NULL) {
      SDL_Log("Error : SDL window creation - %s\n", SDL_GetError());
      SDL_Quit();
      exit(EXIT_FAILURE);
    }
    k++;
    SDL_Delay(100);
  }

  for(int j = 0; j < 10; j++) {
    windowTab[k] = SDL_CreateWindow("fentrelambda", 900, j * 100,
              width, height,
              SDL_WINDOW_RESIZABLE);
    if (windowTab[k] == NULL) {
      SDL_Log("Error : SDL window creation - %s\n", SDL_GetError());
      SDL_Quit();
      exit(EXIT_FAILURE);
    }
    k++;
    windowTab[k] = SDL_CreateWindow("fentrelambda", j * 100, 900,
              width, height,
              SDL_WINDOW_RESIZABLE);
    if (windowTab[k] == NULL) {
      SDL_Log("Error : SDL window creation - %s\n", SDL_GetError());
      SDL_Quit();
      exit(EXIT_FAILURE);
    }
    k++;
    SDL_Delay(100);
  }

  for(int i = 0; i < 10; i++) {
    windowTab[k] = SDL_CreateWindow("fentrelambda", i * 100, i * 100,
              width, height,
              SDL_WINDOW_RESIZABLE);
    if (windowTab[k] == NULL) {
      SDL_Log("Error : SDL window creation - %s\n", SDL_GetError());
      SDL_Quit();
      exit(EXIT_FAILURE);
    }
    k++;
    windowTab[k] = SDL_CreateWindow("fentrelambda", 900 - i * 100, i * 100,
              width, height,
              SDL_WINDOW_RESIZABLE);
    if (windowTab[k] == NULL) {
      SDL_Log("Error : SDL window creation - %s\n", SDL_GetError());
      SDL_Quit();
      exit(EXIT_FAILURE);
    }
    k++;
    SDL_Delay(100);
  }
  SDL_bool program_on = SDL_FALSE;
  SDL_Event event;
  while (program_on){
    if (SDL_PollEvent(&event)){
      switch(event.type){
      case SDL_QUIT :
        program_on = SDL_FALSE;
        break;
      default:
        break;
      }
    }
  }
  SDL_Delay(100);

  SDL_Delay(2000);
  for(int i = 0; i < k; i++) {
    SDL_DestroyWindow(windowTab[i]);
    SDL_Delay(100);
  }

  SDL_Quit();

  return 0;
}
