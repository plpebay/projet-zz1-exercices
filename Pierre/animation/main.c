#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

SDL_Color background = {20, 20, 215, 255};

void end_sdl(char ok,                                               // fin normale : ok = 0 ; anormale ok = 1
             char const* msg,                                       // message à afficher
             SDL_Window* window,                                    // fenêtre à fermer
             SDL_Renderer* renderer) {                              // renderer à fermer
  char msg_formated[255];
  int l;

  if (!ok) {                                                        // Affichage de ce qui ne va pas
    strncpy(msg_formated, msg, 250);
    l = strlen(msg_formated);
    strcpy(msg_formated + l, " : %s\n");

    SDL_Log(msg_formated, SDL_GetError());
  }

  if (renderer != NULL) {                                           // Destruction si nécessaire du renderer
    SDL_DestroyRenderer(renderer);                                  // Attention : on suppose que les NULL sont maintenus !!
    renderer = NULL;
  }
  if (window != NULL)   {                                           // Destruction si nécessaire de la fenêtre
    SDL_DestroyWindow(window);                                      // Attention : on suppose que les NULL sont maintenus !!
    window= NULL;
  }

  SDL_Quit();

  if (!ok) {
    exit(EXIT_FAILURE);
  }
}

void play_with_texture_1(SDL_Texture *background1, SDL_Window *window,
                         SDL_Renderer *renderer) {
  SDL_Rect
          source = {0},                         // Rectangle définissant la zone de la texture à récupérer
          window_dimensions = {0},              // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
          destination = {0};                    // Rectangle définissant où la zone_source doit être déposée dans le renderer

  SDL_GetWindowSize(
      window, &window_dimensions.w,
      &window_dimensions.h);                    // Récupération des dimensions de la fenêtre
  SDL_QueryTexture(background1, NULL, NULL,
                   &source.w, &source.h);       // Récupération des dimensions de l'image

  destination = window_dimensions;              // On fixe les dimensions de l'affichage à  celles de la fenêtre

  /* On veut afficher la texture de façon à ce que l'image occupe la totalité de la fenêtre */

  SDL_RenderCopy(renderer, background1,
                 &source,
                 &destination);                 // Création de l'élément à afficher
}

int main() {
  SDL_Window *window;
  SDL_Renderer *renderer;
  if (SDL_CreateWindowAndRenderer(1000, 344, 0, &window,
                                  &renderer) < 0) {
      SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
                   "Create window and renderer: %s", SDL_GetError());
      return EXIT_FAILURE;
  }
  SDL_SetWindowTitle(window, "Animation");

  SDL_Texture *background1;
  background1 = IMG_LoadTexture(renderer,"background.png");
  if (background1 == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", window, renderer);
  SDL_Texture *background2;
  background2 = IMG_LoadTexture(renderer,"background-mountains.png");
  if (background2 == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", window, renderer);
  SDL_Texture *megaman;
  megaman = IMG_LoadTexture(renderer,"megaman.png");
  if (megaman == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", window, renderer);

  SDL_Rect
    source = {0},                             // Rectangle définissant la zone de la texture à récupérer
    window_dimensions = {0},                  // Rectangle définissant la fenêtre, on  n'utilisera que largeur et hauteur
    destination = {0};                        // Rectangle définissant où la zone_source doit être déposée dans le renderer

  SDL_GetWindowSize(window,                   // Récupération des dimensions de la fenêtre
                    &window_dimensions.w,
                    &window_dimensions.h);
  SDL_QueryTexture(megaman, NULL, NULL,    // Récupération des dimensions de l'image
                   &source.w, &source.h);

  int nb_images = 5;                         //  Il y a 8 vignette dans la ligne qui nous intéresse
  int nb_images_animation = 8 * nb_images;    //
  float zoom = 1;                             // zoom, car ces images sont un peu petites
  int offset_x = source.w / 5,                // La largeur d'une vignette de l'image
      offset_y = source.h / 2;                // La hauteur d'une vignette de l'image
  SDL_Rect state[nb_images_animation];                         // Tableau qui stocke les vignettes dans le bon ordre pour l'animation

  /* construction des différents rectangles autour de chacune des vignettes de la planche */
  int i = 0;
  for (int x = 0; x < source.w; x += offset_x) {
    state[i].x = x;
    state[i].y = 0;
    state[i].w = offset_x;
    state[i].h = offset_y;
    ++i;
  }
  for(int k = 5; k < 40; k++){
    state[k] = state[k - 5];
  }

  destination.w = offset_x * zoom;            // Largeur du sprite à l'écran
  destination.h = offset_y * zoom;            // Hauteur du sprite à l'écran
  destination.x = window_dimensions.w * 0.01; // Position en x pour l'affichage du sprite
  destination.y = window_dimensions.h * 0.5;  // Position en y pour l'affichage du sprite

  SDL_bool quit = SDL_FALSE;
  i = 0;
  int cpt_animation = 0;
  while (!quit) {
      SDL_Event event;
      SDL_bool trouve = SDL_FALSE;
        while (!trouve && SDL_PollEvent(&event)) { // gestion clavier souris
          switch (event.type) {
            case SDL_KEYDOWN:
              switch(event.key.keysym.sym){
                default:
                  break;
              }
            break;
          case SDL_QUIT:
              quit = SDL_TRUE;
              trouve = SDL_TRUE;
              break;
          }
      }
      SDL_RenderClear(renderer);
      play_with_texture_1(background1,         // identique à play_with_texture_1, où on a enlevé l'affichage et la pause
                            window, renderer);
      play_with_texture_1(background2,         // identique à play_with_texture_1, où on a enlevé l'affichage et la pause
                            window, renderer);

      destination.x = cpt_animation * 20;
      SDL_RenderCopy(renderer,                  // Préparation de l'affichage
                     megaman, &state[i], &destination);
      i = (i + 1) % nb_images;
      cpt_animation = (cpt_animation + 1) % nb_images_animation;
      //play_with_texture_5(background1, background2, megaman, window, renderer);
      SDL_RenderPresent(renderer);              // Affichage
      SDL_Delay(100);
      SDL_PumpEvents();


  }
  SDL_DestroyTexture(background1);
  SDL_DestroyTexture(background2);
  SDL_DestroyTexture(megaman);
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  IMG_Quit();
  SDL_Quit();
  return 0;
}
