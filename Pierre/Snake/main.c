#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

void end_sdl(char ok,                                               // fin normale : ok = 0 ; anormale ok = 1
             char const* msg,                                       // message à afficher
             SDL_Window* window,                                    // fenêtre à fermer
             SDL_Renderer* renderer) {                              // renderer à fermer
  char msg_formated[255];
  int l;

  if (!ok) {                                                        // Affichage de ce qui ne va pas
    strncpy(msg_formated, msg, 250);
    l = strlen(msg_formated);
    strcpy(msg_formated + l, " : %s\n");

    SDL_Log(msg_formated, SDL_GetError());
  }

  if (renderer != NULL) {                                           // Destruction si nécessaire du renderer
    SDL_DestroyRenderer(renderer);                                  // Attention : on suppose que les NULL sont maintenus !!
    renderer = NULL;
  }
  if (window != NULL)   {                                           // Destruction si nécessaire de la fenêtre
    SDL_DestroyWindow(window);                                      // Attention : on suppose que les NULL sont maintenus !!
    window= NULL;
  }

  SDL_Quit();

  if (!ok) {
    exit(EXIT_FAILURE);
  }
}

void draw(SDL_Renderer* renderer) {
  SDL_Rect rectangle;
  SDL_SetRenderDrawColor(renderer,
                         rand() % 255, rand() % 255, rand() % 255,
                         255);
  SDL_DisplayMode screen;
  SDL_GetCurrentDisplayMode(0, &screen);
  rectangle.x = rand() % 1000 + rand() % 100 - rand() % 100;                                        // x haut gauche du rectangle
  rectangle.y = rand() % 500 + rand() % 100 - rand() % 100;                                      // y haut gauche du rectangle
  rectangle.w = rand() % 100 + 20;                                           // sa largeur (w = width)
  rectangle.h = rand() % 100 + 20;                                                // sa hauteur (h = height)

  SDL_RenderFillRect(renderer, &rectangle);
}

int main(int argc, char** argv) {
  (void)argc;
  (void)argv;
  time_t t;

  SDL_Window* window = NULL;
  SDL_Renderer* renderer = NULL;

  SDL_DisplayMode screen;

  SDL_bool program_on = SDL_TRUE;
  SDL_Event event;

  /*********************************************************************************************************************/
  /*                         Initialisation de la SDL  + gestion de l'échec possible                                   */
  if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", window, renderer);

  SDL_GetCurrentDisplayMode(0, &screen);
  printf("Résolution écran\n\tw : %d\n\th : %d\n",
         screen.w, screen.h);

  /* Création de la fenêtre */
  window = SDL_CreateWindow("Premier dessin",
                            SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED, 1000,
                            500,
                            SDL_WINDOW_OPENGL);
  if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

  /* Création du renderer */
  renderer = SDL_CreateRenderer(window, -1,
                                SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

  /*********************************************************************************************************************/
  /*                                     On dessine dans le renderer                                                   */
  /*********************************************************************************************************************/
  /*             Cette partie pourrait avantageusement être remplacée par la boucle évènementielle                     */
  int chance;
  while (program_on){
    srand((unsigned) time(&t));
    if (SDL_PollEvent(&event)){
      switch(event.type){
      case SDL_QUIT :
        program_on = SDL_FALSE;
        break;
      default:
        break;
      }
    }
    chance = rand() % 10;
    if(chance == 2){
      SDL_RenderClear(renderer);
    }
    draw(renderer);
    SDL_RenderPresent(renderer);
    SDL_Delay(300);
  }

  /* on referme proprement la SDL */
  end_sdl(1, "Normal ending", window, renderer);
  return EXIT_SUCCESS;
}
