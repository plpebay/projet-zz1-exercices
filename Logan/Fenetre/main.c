#include <SDL2/SDL.h>
#include <stdio.h>

/****************************************/
/* Création de fenêtres sous forme de Z */
/****************************************/

int main(int argc, char **argv) {
    (void)argc;
    (void)argv;
    int i;
    int j;

    SDL_Window *window_tab[90];                            // Tableau de fenêtres
                
    for (i=0; i<90; i++){
        
        /* Initialisation de la SDL  + gestion de l'échec possible */
        if (SDL_Init(SDL_INIT_VIDEO) != 0) {
            SDL_Log("Error : SDL initialisation - %s\n", 
                    SDL_GetError());                       // l'initialisation de la SDL a échoué 
            exit(EXIT_FAILURE);
        }

        if(i<30){
            /* Création de la barre du haut du Z */
            window_tab[i] = SDL_CreateWindow(
                "Fenêtre du ZZ",                          // codage en utf8, donc accents possibles
                20*i, 0,                                  // coin haut gauche en haut gauche de l'écran
                400, 100,                                 // largeur = 400, hauteur = 100
                SDL_WINDOW_RESIZABLE);                    // redimensionnable

            SDL_Delay(10);                                // Pause exprimée  en ms
        }
        
        else if(i<60){
            /* Création de la barre diagonale du Z */
            window_tab[i] = SDL_CreateWindow(
                "Fenêtre du ZZ",                    
                600-20*(i-30), 20*(i-30),           
                400, 100,                           
                SDL_WINDOW_RESIZABLE);              
            SDL_Delay(10);                          
        }
        
        else{
            /* Création de la barre du bas du Z */
            window_tab[i]= SDL_CreateWindow(
                "Fenêtre du ZZ",                    
                20*(i-60), 600,                     
                400, 100,                           
                SDL_WINDOW_RESIZABLE);              
            SDL_Delay(10);                          
        }

        if (window_tab[i] == NULL) {
            SDL_Log("Error : SDL window %d creation - %s\n",i, 
                    SDL_GetError());                 // échec de la création de la fenêtre
            SDL_Quit();                              // On referme la SDL       
            exit(EXIT_FAILURE);
        }


        }

    for(j=0; j<40; j++){                            // boucle de 40 déplacements des fenêtres.
        for(i=0; i<90; i++){                        // parcours des fenêtres.
            if(i<30){
                SDL_SetWindowPosition(              //deplacement en bas à droite de la barre du haut du Z.
                    window_tab[i],
                        20*i+10*j,
                        5*j);    
            }
        
            else if(i<60){
                SDL_SetWindowPosition(              //deplacement en bas à droite de la barre diagonale du Z.
                    window_tab[i],
                    600-20*(i-30)+10*j,
                    20*(i-30)+5*j);  
            }
            
            else{
            SDL_SetWindowPosition(                  //deplacement en bas à droite de la barre du bas du Z.
                window_tab[i],
                20*(i-60)+10*j,
                600+5*j);   
            }
        }
        
    }
    for(i=0;i<90;i++){
            SDL_SetWindowPosition(window_tab[i],1200 ,400);
            SDL_Delay(10);                           
        }
        /* On referme tout ce qu'on a ouvert dans l'ordre de création */
    for(i=0; i<90; i++){
        SDL_DestroyWindow(window_tab[i]);               
            
    }
    SDL_Quit();                                // On quitte la SDL
        
    
  return 0;
}
