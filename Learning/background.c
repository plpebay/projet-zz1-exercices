#include "background.h"

/*========================Space Background Display============================*/
/*                                                                            */
void drawSpace(SDL_Window* window, SDL_Renderer* renderer){
  SDL_Rect window_dimensions = {0};
  SDL_Rect rectangle;

  SDL_GetWindowSize(
      window, &window_dimensions.w,
      &window_dimensions.h);
  rectangle.x = 0;
  rectangle.y = 0;
  rectangle.w = window_dimensions.w;
  rectangle.h = window_dimensions.h;

  SDL_SetRenderDrawColor(renderer, 15, 10, 40, 255);
  SDL_RenderFillRect(renderer, &rectangle);
}
/*========================Stars Background Display============================*/
/*                                                                            */
void drawStars(SDL_Window* window, SDL_Renderer* renderer){
  int size_point = 5;

  SDL_Rect window_dimensions = {0};

  SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);

  SDL_SetRenderDrawColor(renderer, 255, 255, 255, 180);

  for (int i = 0; i < 40 ; i++){                                                // Creating random points inside a disk
    int rand_1 = rand() % window_dimensions.w-size_point;
    int rand_2 = rand() % window_dimensions.h-size_point;
    for (int j=0; j<size_point; j++){
      for (int k=0; k<size_point; k++){
        SDL_RenderDrawPoint(renderer, rand_1 + j, rand_2 + k);
      }
    }
  }
}
/*================Combine Space & Stars Background Display====================*/
/*                                                                            */
void drawBackground(SDL_Window* window, SDL_Renderer* renderer){
  drawSpace(window, renderer);
  drawStars(window, renderer);
}
