#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <SDL2/SDL_ttf.h>

void Text(SDL_Window *window, SDL_Renderer *renderer, TTF_Font* font,
          char *word);
void TextWrite(SDL_Window *window, SDL_Renderer *renderer, TTF_Font* font,
                char string[10], int i, char initscore[7], char score[18]);
void menu(SDL_Renderer* renderer, SDL_Window* window);
void score(SDL_Renderer* renderer, SDL_Window* window);
void scoreGO(SDL_Renderer* renderer, SDL_Window* window);
void GOmenu(SDL_Renderer* renderer, SDL_Window* window);
