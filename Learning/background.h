#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

void drawSpace(SDL_Window* window, SDL_Renderer* renderer);
void drawStars(SDL_Window* window, SDL_Renderer* renderer);
void drawBackground(SDL_Window* window, SDL_Renderer* renderer);
