#include "asteroids.h"

void showAsteroids(int matrix[6],
                           SDL_Texture *my_texture,
                           SDL_Window *window,
                           SDL_Renderer *renderer,
                           SDL_Rect Shipdestination,
                           int animationOffset,
                           int j) {

  SDL_Rect
    source = {0},                             // Rectangle définissant la zone de la texture à récupérer
    window_dimensions = {0},                  // Rectangle définissant la fenêtre, on  n'utilisera que NBWIDTH et hauteur
    destination = {-200};                     // Rectangle définissant où la zone_source doit être déposée dans le renderer
  SDL_Rect
    source2 = {0},
    destination2 = {-200};
  SDL_Rect
    source3 = {0},
    destination3 = {-200};
  SDL_Rect
    source4 = {0},
    destination4 = {-200};
  SDL_Rect
    source5 = {0},
    destination5 = {-200};
  SDL_Rect
    source6 = {0},
    destination6 = {-200};
  SDL_Rect
    source7 = {0},
    destination7 = {-200};


  SDL_GetWindowSize(window,                   // Récupération des dimensions de la fenêtre
                  &window_dimensions.w,
                  &window_dimensions.h);

  int i;
  
  // Récupération des dimensions de l'image
  SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h);
  SDL_QueryTexture(my_texture, NULL, NULL, &source2.w, &source2.h);
  SDL_QueryTexture(my_texture, NULL, NULL, &source3.w, &source3.h);
  SDL_QueryTexture(my_texture, NULL, NULL, &source4.w, &source4.h);
  SDL_QueryTexture(my_texture, NULL, NULL, &source5.w, &source5.h);
  SDL_QueryTexture(my_texture, NULL, NULL, &source6.w, &source6.h);
  SDL_QueryTexture(my_texture, NULL, NULL, &source7.w, &source7.h);

  float zoom = 0.3;                       // zoom, car ces images sont un peu petites
  int offset_x = source.w,                // La NBWIDTH d'une vignette de l'image
      offset_y = source.h;                // La hauteur d'une vignette de l'image


  destination.w = offset_x * zoom;            // NBWIDTH du sprite à l'écran
  destination.h = offset_y * zoom;            // Hauteur du sprite à l'écran
  destination2.w = offset_x * zoom;            
  destination2.h = offset_y * zoom;            
  destination3.w = offset_x * zoom;            
  destination3.h = offset_y * zoom;            
  destination4.w = offset_x * zoom;            
  destination4.h = offset_y * zoom;            
  destination5.w = offset_x * zoom;           
  destination5.h = offset_y * zoom;           
  destination6.w = offset_x * zoom;           
  destination6.h = offset_y * zoom;            
  destination7.w = offset_x * zoom;            
  destination7.h = offset_y * zoom;            

  int gap1 = window_dimensions.w/(int)(2*NBWIDTH) - destination.w/2;
  int gap2 = window_dimensions.w/(int)(2*NBWIDTH) - destination2.w/2;
  int gap3 = window_dimensions.w/(int)(2*NBWIDTH) - destination3.w/2;
  int gap4 = window_dimensions.w/(int)(2*NBWIDTH) - destination4.w/2;
  int gap5 = window_dimensions.w/(int)(2*NBWIDTH) - destination5.w/2;
  int gap6 = window_dimensions.w/(int)(2*NBWIDTH) - destination6.w/2;
  int gap7 = window_dimensions.w/(int)(2*NBWIDTH) - destination7.w/2;

  int width_column = window_dimensions.w / NBWIDTH;
  int height_line = window_dimensions.h / 6;

  int tab_pos[NBWIDTH] = {0};
  codeBin(matrix[j], tab_pos);


  for(i = 0; i < NBWIDTH; i++){
      switch(i){

      case 0:
        if (tab_pos[i]==1){
          destination.x = width_column * i + gap1;                  // Position en x pour l'affichage du sprite 1
          destination.y = height_line * (j-1) + animationOffset;    // Position en y pour l'affichage du sprite 1
        }
      break;
      case 1:
        if (tab_pos[i]==1){
          destination2.x = width_column * i + gap2;                  // Position en x pour l'affichage du sprite 2
          destination2.y = height_line * (j-1) + animationOffset;    // Position en y pour l'affichage du sprite 2
        }
      break;
      case 2:
        if (tab_pos[i]==1){
          destination3.x = width_column * i + gap3;                  
          destination3.y = height_line * (j-1) + animationOffset;    
        }
      break;
      case 3:
        if (tab_pos[i]==1){
          destination4.x = width_column * i + gap4;                  
          destination4.y = height_line * (j-1) + animationOffset;    
        }
      break;
      case 4:
        if (tab_pos[i]==1){
          destination5.x = width_column * i + gap5;                  
          destination5.y = height_line * (j-1) + animationOffset;    
        }
      break;
      case 5:
        if (tab_pos[i]==1){
          destination6.x = width_column * i + gap6;                  
          destination6.y = height_line * (j-1) + animationOffset;    
        }
      break;
      case 6:
        if (tab_pos[i]==1){
          destination7.x = width_column * i + gap7;                  
          destination7.y = height_line * (j-1) + animationOffset;    
        }
      break;
      default:
      break;
      }

  }

  if(matrix[j] != 0){
    // Préparation de l'affichage
    SDL_RenderCopy(renderer, my_texture, &source, &destination);
    SDL_RenderCopy(renderer, my_texture, &source2, &destination2);
    SDL_RenderCopy(renderer, my_texture, &source3, &destination3);
    SDL_RenderCopy(renderer, my_texture, &source4, &destination4);
    SDL_RenderCopy(renderer, my_texture, &source5, &destination5);
    SDL_RenderCopy(renderer, my_texture, &source6, &destination6);
    SDL_RenderCopy(renderer, my_texture, &source7, &destination7);
  }
}
