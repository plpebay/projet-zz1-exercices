#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include "SDLmonitor.h"
#include "background.h"
#include "asteroids.h"
#include "graphics.h"
#include "play.h"


#define NBLINE 5

//pow(2,NBWIDTH)-1
#define NBETAT 63
#define NBPOS NBWIDTH
#define NBAVGCOLUMN 3
//NBWIDTH-2
#define NBTIC  4
#define MAXTRY 500

void initMat(double * mat);
int computeNextEtat(double m[(int)(pow(2,NBWIDTH)-1)], int etat);
void updateMatrix(int matrix[6], int etat);
void updateMatrix_0(int matrix[6]);
void initMatrix(int matrix[6],double m[(int)(pow(2,NBWIDTH)-1)]);
void printMatrix(int matrix[6]);
void codeBin(int nb, int nb_bin[NBWIDTH]);
