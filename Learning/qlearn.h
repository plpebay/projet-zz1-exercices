#include "game.h"

void initQtable(double table[NBPOS * NBETAT * NBAVGCOLUMN * NBTIC][3]);
int easyColumn(int matrix[6]);
void updateQtable(double qtable[NBPOS * NBETAT * NBAVGCOLUMN * NBTIC][3],
                  int s[MAXTRY], // State
                  int a[MAXTRY], // Action
                  int r[MAXTRY], // Reward
                  int n, // Number of states, actions and rewards saved
                  double gamma,
                  double ksi);
void exportQtable(double qtable[NBPOS * NBETAT * NBAVGCOLUMN * NBTIC][3], char* filename);
void importQtable(double qtable[NBPOS * NBETAT * NBAVGCOLUMN * NBTIC][3], char* filename);
void addQtableAvgToFile(double qtable[NBPOS * NBETAT * NBAVGCOLUMN * NBTIC][3], char* filename);
void addScoreToFile(int score, char* filename);
