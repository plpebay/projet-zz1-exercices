
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <SDL2/SDL_ttf.h>

void end_sdl(char ok, char const* msg, SDL_Window* window, SDL_Renderer* renderer);
void initSDL();
