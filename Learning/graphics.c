#include "graphics.h"
#include "SDLmonitor.h"

/*======================Score Menu Rectangle Display==========================*/
/*   Display white transparent rectangle at the top of screen for score text  */
void score(SDL_Renderer* renderer, SDL_Window* window){

    SDL_Rect window_dimensions = {0};

    SDL_GetWindowSize(window, &window_dimensions.w,&window_dimensions.h);

    SDL_Rect score;

    score.x = 0;
    score.y = 0;
    score.w = window_dimensions.w;
    score.h = window_dimensions.h/30;

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 200);
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

    SDL_RenderFillRect(renderer, &score);

}
/*==================Score Text Display when no increments=====================*/
/*                                                                            */
void Text(SDL_Window *window, SDL_Renderer *renderer, TTF_Font* font,
          char *word){

  SDL_Color color = {0, 0, 0, 255};                                             // text color
  SDL_Surface* text_surface = NULL;                                             // surface (intermediate)

  text_surface = TTF_RenderText_Blended(font, word, color);                     // text creation in surface
  if (text_surface == NULL) end_sdl(0, "Can't create text surface", window,
                                    renderer);

  SDL_Texture* text_texture = NULL;                                             // texture containing the text
  text_texture = SDL_CreateTextureFromSurface(renderer, text_surface);          // transfert surface to texture
  if (text_texture == NULL) end_sdl(0, "Can't create texture from surface",
                                    window, renderer);
  SDL_FreeSurface(text_surface);

  SDL_Rect pos = {0, 0, 0, 0};                                                  // rectangle where the text will be displayed
  SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);                   // size (w, h) recovery from text
  SDL_RenderCopy(renderer, text_texture, NULL, &pos);                           // text writting inside the renderer

}
/*====================Score Text Display when increments======================*/
/*                                                                            */
void TextWrite(SDL_Window *window, SDL_Renderer *renderer, TTF_Font* font,
                char string[10], int i, char initscore[7], char score[18]){
    sprintf(string, "%d", i);
    strcat(score,string);
    Text(window, renderer, font, score);
    memset(score, 0, strlen(score));
    strcat(score,initscore);
}
/*======================Start Menu Rectangle Display==========================*/
/*                                                                            */
void menu(SDL_Renderer* renderer, SDL_Window* window){

    SDL_Rect window_dimensions = {0};

    SDL_GetWindowSize(window, &window_dimensions.w,&window_dimensions.h);

    SDL_Rect menu;

    menu.x = window_dimensions.w/10;
    menu.y = window_dimensions.h/10;
    menu.w = 8*window_dimensions.w/10;
    menu.h = 6*window_dimensions.h/10;

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 150);
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

    SDL_RenderFillRect(renderer, &menu);
}
/*====================Game Over Menu Rectangle Display========================*/
/*                                                                            */
void GOmenu(SDL_Renderer* renderer, SDL_Window* window){

    SDL_Rect window_dimensions = {0};

    SDL_GetWindowSize(window, &window_dimensions.w,&window_dimensions.h);

    SDL_Rect menu;

    menu.x = 0;
    menu.y = 0;
    menu.w = window_dimensions.w;
    menu.h = window_dimensions.h;

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

    SDL_RenderFillRect(renderer, &menu);
}
