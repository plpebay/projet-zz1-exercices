#include "qlearn.h"


void initQtable(double table[NBPOS * NBETAT * NBAVGCOLUMN * NBTIC][3]){
    int i,j;
    for(i = 0; i < NBPOS * NBETAT * NBAVGCOLUMN * NBTIC; i++){
        for(j = 0; j < 3; j++){
            table[i][j]=(double)0.5;
        }
    }
}

/*===== Function finding the column with the less asteroid=====*/
int easyColumn(int matrix[6]){
    int asteroidPerColumn[NBWIDTH] = {0};
    int tabPosAsteroid[NBWIDTH] = {0};
    for(int i = 1; i < NBLINE; i++){
        codeBin(matrix[i], tabPosAsteroid);
        for(int j = 0; j < NBWIDTH; j++){
            asteroidPerColumn[j] += tabPosAsteroid[j] ;
        }
    }
    int column;
    int min = 100;
    for(int i = 0; i < NBWIDTH; i += 2){
        if(asteroidPerColumn[i] + asteroidPerColumn[i + 1] < min){
            column = i/2;
            min = asteroidPerColumn[i] + asteroidPerColumn[i + 1];
        }
    }
    return column;
}

/*======= Function updating the Qtable =======*/
void updateQtable(double qtable[NBPOS * NBETAT * NBAVGCOLUMN * NBTIC][3],
                  int s[MAXTRY], // State
                  int a[MAXTRY], // Action
                  int r[MAXTRY], // Reward
                  int n, // Number of states, actions and rewards saved
                  double gamma,
                  double ksi){
  double max;
  qtable[s[n - 1]][a[n - 1]] += ksi * (r[n] - qtable[s[n - 1]][a[n - 1]]);
  for(int i = n - 2; i > -1; i--){
    max = 0;
    for(int k = 0; k < 3; k++){ // For all possible actions (0, 1, 2) = (left, nothing, right)
      if(qtable[s[i + 1]][k] > max){
        max = qtable[s[i + 1]][k];
      }
    }
    qtable[s[i]][a[i]] += ksi * (r[i + 1] + gamma * max - qtable[s[i]][a[i]]);
  }
}

/*===Function to save the Qtable in a file in order to draw a graph===*/
void exportQtable(double qtable[NBPOS * NBETAT * NBAVGCOLUMN * NBTIC][3], char* filename){
  FILE* file;
  double value;

  file = fopen(filename, "w");
  if(file == NULL) {
    printf("file can't be opened\n");
    exit(1);
  }

  for(int i =0; i < NBPOS * NBETAT * NBAVGCOLUMN * NBTIC; i++){
    for(int j = 0; j < 3; j++){
      value = qtable[i][j];
      fprintf(file, "%f", value);
      fprintf(file, " ");
    }
    fprintf(file, "\n");
  }

  fclose(file);
}

/*===Function to load the Qtable ===*/
void importQtable(double qtable[NBPOS * NBETAT * NBAVGCOLUMN * NBTIC][3], char* filename){
  FILE* file;
  double value = 0;
  int k = 0;

  file = fopen(filename, "r");
  if(file == NULL) {
    printf("file can't be opened\n");
    exit(1);
  }

  fscanf (file, "%lf", &value);
  while (!feof (file))
    {
      qtable[k / 3][k % 3] = value;
      printf ("%d, %d \n", k / 3, k % 3);
      fscanf (file, "%lf", &value);
      k++;
    }
  fclose(file);
}

/*===Function to save values in a file in order to draw a graph===*/
void addQtableAvgToFile(double qtable[NBPOS * NBETAT * NBAVGCOLUMN * NBTIC][3], char* filename){
  FILE* file;
  double value = 0;

  file = fopen(filename, "a");
  if(file == NULL) {
    printf("file can't be opened\n");
    exit(1);
  }

  for(int i =0; i < NBPOS * NBETAT * NBAVGCOLUMN * NBTIC; i++){
    for(int j = 0; j < 3; j++){
      value += qtable[i][j];
    }
  }

  fprintf(file, "%f\n", value);
  fclose(file);
}

/*===Function to save values in a file in order to draw a graph===*/
void addScoreToFile(int score, char* filename){
  FILE* file;
  file = fopen(filename, "a");
  if(file == NULL) {
    printf("file can't be opened\n");
    exit(1);
  }

  fprintf(file, "%d\n", score);
  fclose(file);
}
