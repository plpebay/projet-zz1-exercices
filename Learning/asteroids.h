#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define NBWIDTH 6

void showAsteroids(int matrix[6],
                        SDL_Texture *my_texture,
                        SDL_Window *window,
                        SDL_Renderer *renderer,
                        SDL_Rect Shipdestination,
                        int animationOffset,
                        int j);
                        
void codeBin(int nb, int nb_bin[NBWIDTH]);