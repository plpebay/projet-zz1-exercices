#include "game.h"

/*======Init each probability to have a state with the same probability=======*/
void initMat(double * mat){
     for (int i=0; i<(pow(2,NBWIDTH)-1); i++){
    mat[i]=100/(double)(pow(2,NBWIDTH)-1); //contain each probability for each state
  }
}

/*===================Return a random new state=========================*/
int computeNextEtat(double m[(int)(pow(2,NBWIDTH)-1)], int etat){
  double val = rand() % 101;
  double cumul = 0;
  int i = 0;
  while(val >= cumul && i < (pow(2,NBWIDTH)-1)){
    cumul += m[i];
    i++;
  }
  if (i != 0){
    etat = i - 1;
  }
  else{
    etat = 0;
  }
  return etat;
}

/*================Update the matrix with the new state======================*/
void updateMatrix(int matrix[6], int etat){
  for(int i = 4; i > -1; i--){
    matrix[i+1] = matrix[i];
  }
  matrix[0] = etat;
}

/*=======Update the matrix with a line without asteroids (state 0)==========*/
void updateMatrix_0(int matrix[6]){
  for(int i = 4; i > -1; i--){
    matrix[i+1] = matrix[i];
  }
  matrix[0] = 0;
}

/*===============Init a first matrix with random states=====================*/
void initMatrix(int matrix[6],double m[(int)(pow(2,NBWIDTH)-1)]){
    int i;
    for(i = 0; i < 3; i++){
      updateMatrix_0(matrix);
      updateMatrix(matrix,computeNextEtat(m,matrix[i]));
    }
}

/*========================Print Function==============================*/
void printMatrix(int matrix[6]){
  int i;
  for(i = 0; i < 6; i++){
    printf("%d ",matrix[i]);
  }
  printf("\n");
}

/*===Convert the state into a binary number where 1 represent an asteroid===*/
void codeBin(int nb, int nb_bin[NBWIDTH]){
  int i;
  for(i = 0; i < NBWIDTH; i++){
    nb_bin[i] = 0;
  }
  for(i = 0; i < NBWIDTH; i++){
    if(nb >= (int)pow(2,NBWIDTH - i - 1)){
      nb_bin[i] = 1;
      nb -= 1 * (int)pow(2,NBWIDTH - i - 1);
    }
  }
}
