import matplotlib.pyplot as plt
import sys

def main():
    args = sys.argv[1:]
    for arg in args:
        X = []
        Y = []
        i = 0
        file = open(arg, 'r')
        lines = file.readlines()
        for line in lines:
            Y.append(float(line.replace("\n","")))
            X.append(i)
            i+=1
        #print(X, Y)
        plt.plot(X,Y)
        plt.show()
        file.close()

if __name__=="__main__":
    main()
