#include "qlearn.h"

void displayRun(SDL_Window *window, SDL_Renderer *renderer,
                SDL_Texture *asteroid, SDL_Texture *character,
                SDL_Texture *menutxt,
                SDL_Texture *GOtxt, // Game Over overlay
                TTF_Font *font,
                double qtable[NBPOS * NBETAT * NBAVGCOLUMN * NBTIC][3]) {

  /*=======================Variables declaration=========================*/
  int width_window = 100 * NBWIDTH; // width of the window
  int height_window = 800;          // height of the window
  int nb_images = 4;                // number of frame for the ship
  int ScoreInc = 0;                 // variable for each tic to show the "score"
  int delay = 50;                  // for fps
  int sdlTic = 0;
  int sdlDistanceTic = 0;
  int line0 = 0;
  int column;
  int etat;
  int offsetAnimation = 0;
  int cpt = 0;     //
  int shippos = 2; // describe the n° of the column where the ship should be
  int offsetRefresh = 0;
  int maxOffset = 10;
  int memoryShipPos;
  SDL_bool quit = SDL_FALSE;
  SDL_bool paused = SDL_TRUE;
  int GameOver = 0;
  int clock = 0;
  int matrix[6] = {0, 0, 0, 0, 0, 0};
  double mat[(int)(pow(2, NBWIDTH) - 1)];
  initMat(mat);
  int tabPosAsteroid[NBWIDTH] = {0};
  float zoom = 0.3;
  char TextScore[18] = "Score:";
  char InitScore[7] = "Score:";
  char ScoreString[10];
  int state = 0;
  SDL_Rect window_dimensions = {0}, source = {0}, stateRect = {0},
           destination = {0}, sourcetxt = {0}, destinationtxt = {0},
           sourceGO = {0}, destinationGO = {0};

  /*=================== Init SDL and some variables =========================*/

  if (TTF_Init() < 0) {
    end_sdl(0, "Couldn't initialize SDL TTF", window, renderer);
  }
  if (SDL_CreateWindowAndRenderer(width_window, height_window, 0, &window,
                                  &renderer) < 0) {
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Create window and renderer: %s",
                 SDL_GetError());
  }
  SDL_SetWindowTitle(window, "Asteroid");
  SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
  asteroid = IMG_LoadTexture(renderer, "asteroid.png");
  character = IMG_LoadTexture(renderer, "ship.png");
  menutxt = IMG_LoadTexture(renderer, "menu.png");
  GOtxt = IMG_LoadTexture(renderer, "GameOver.png");
  font = TTF_OpenFont("Space-font.ttf", 25); // La police à charger, la taille désirée
  if (GOtxt == NULL || menutxt == NULL || character == NULL || asteroid == NULL)
    end_sdl(0, "Echec du chargement de l'image dans une texture", window,
            renderer);
  if (font == NULL)
    end_sdl(0, "Can't load font", window, renderer);
  TTF_SetFontStyle(font, TTF_STYLE_ITALIC | TTF_STYLE_BOLD);
  SDL_QueryTexture(character, NULL, NULL, &source.w, &source.h);
  SDL_QueryTexture(menutxt, NULL, NULL, &sourcetxt.w, &sourcetxt.h);
  SDL_QueryTexture(GOtxt, NULL, NULL, &sourceGO.w, &sourceGO.h);

  int offset_x = source.w / nb_images, offset_y = source.h;
  stateRect.x = 0;
  stateRect.y = 0;
  stateRect.w = offset_x;
  stateRect.h = offset_y;
  destination.w = offset_x * zoom;
  destination.h = offset_y * zoom;
  destination.x = (window_dimensions.w - destination.w) / 2;
  destination.y =
      9 * (window_dimensions.h - destination.h) / 10 +
      75; // On place le vaisseau au 9 /10 de l'écran avec 75 pixels d'offset
  destinationtxt.w = sourcetxt.w * 0.6;
  destinationtxt.h = sourcetxt.h * 0.6;
  destinationtxt.x = window_dimensions.w / 2 - destinationtxt.w / 2;
  destinationtxt.y = window_dimensions.h / 3 - destinationtxt.h / 3;
  destinationGO.w = sourceGO.w * 0.5;
  destinationGO.h = sourceGO.h * 0.5;
  destinationGO.x = window_dimensions.w / 2 - destinationGO.w / 2;
  destinationGO.y = window_dimensions.h / 2 - destinationGO.h / 2;

  /*====================== Updating variables ==========================*/

  while (!quit) {
    SDL_Event event;
    while (SDL_PollEvent(&event)) { // gestion clavier souris
      switch (event.type) {
      case SDL_KEYDOWN:
        switch (event.key.keysym.sym) {
        case SDLK_ESCAPE:
          quit = SDL_TRUE;
          break;
        case SDLK_SPACE:
          paused = !paused;
          break;
        default:
          break;
        }
        break;
      case SDL_QUIT:
        quit = SDL_TRUE;
        break;
      }
    }

    if(!paused){
      destination.x = Position(window_dimensions, destination, memoryShipPos,
                              offsetRefresh); // Ship Position
      if (memoryShipPos > shippos) {
        offsetRefresh--;
      } 
      else if (memoryShipPos < shippos) {
        offsetRefresh++;
      }
      SDL_RenderClear(renderer); // reset renderer
      drawBackground(window, renderer); // display background (space and stars)
      for (int j = 0; j < 6; j++) { //display asteroids
        showAsteroids(matrix, asteroid, window, renderer, destination, offsetAnimation, j);
      }
      offsetAnimation += 3;
      stateRect.x += offset_x;      // update ship animation
      stateRect.x %= source.w;      // reset ship animation
      SDL_RenderCopy(renderer, character, &stateRect, &destination); // display ship
      TextWrite(window, renderer, font, ScoreString, ScoreInc, InitScore,
                    TextScore); // display score text
    }
    else{
      SDL_RenderClear(renderer); // reset renderer
      drawBackground(window, renderer); // display background (space and stars)
      SDL_RenderCopy(renderer, character, &stateRect,
                    &destination); // display ship
      stateRect.x += offset_x;      // update ship animation
      stateRect.x %= source.w;      // reset ship animation
    }
    if (GameOver) {
      GOmenu(renderer, window); // display game over background
      SDL_RenderCopy(renderer, GOtxt, &sourceGO,
                     &destinationGO); // display game over text
      TextWrite(window, renderer, font, ScoreString, ScoreInc, InitScore,
                TextScore); // display score text
      if (paused) {                  // press to restart the game
        for (int i = 0; i <= 5; i++){ // reset the asteroid matrix
          matrix[i] = 0;
        }
        ScoreInc = 0;         // reset score
        GameOver = !GameOver; // exit game over
        shippos = 2;          // reset ship position
      }
    }
    else {
      score(renderer, window); // display score rectangle (top of the screen)
      if (paused) {
        menu(renderer, window); // display pause background
        SDL_RenderCopy(renderer, menutxt, &sourcetxt, &destinationtxt); // display pause text
        TextWrite(window, renderer, font, ScoreString, ScoreInc, InitScore,
                TextScore); // display score text
      }
      else {
        TextWrite(window, renderer, font, ScoreString, ScoreInc, InitScore,
                  TextScore); // display score text
        if (clock % maxOffset == 0) {
          ScoreInc++; // increment score
          offsetRefresh = 0;
          if ((sdlTic % 4) == 0) {
            sdlDistanceTic = 0;
            offsetAnimation = 0;
            etat = computeNextEtat(mat, etat);
            if (!line0) {
              updateMatrix(matrix, etat);
              line0 = 1;
            } 
            else {
              updateMatrix_0(matrix);
              line0 = 0;
            }
          } 
          else {
            sdlDistanceTic++;
          }
          column = easyColumn(matrix);
          state = matrix[5] * NBPOS * NBAVGCOLUMN * NBTIC +
                  shippos * NBAVGCOLUMN * NBTIC + column * NBTIC +
                  sdlDistanceTic;
          
          float max = qtable[state][0];
          int displace = 0;
          for (int i = 1; i < 3; i++) {
            if (qtable[state][i] >= max) {
              max = qtable[state][i];
              displace = i;
            }
          }
          memoryShipPos = shippos;  //register the last ship position
          switch (displace) {      //update the new ship position
          case 0:
            if (shippos != 0) {
              shippos--;           
            }
            break;
          case 2:
            if (shippos != 5) {
              shippos++;
            }
            break;
          }

          codeBin(matrix[5], tabPosAsteroid);

          if ((sdlDistanceTic == 3) && tabPosAsteroid[shippos]) {
            GameOver = 1;
          }

          sdlTic++;
          cpt++;
        }
      }
    }
    SDL_RenderPresent(renderer); // display all renderer
    SDL_Delay(delay);
    SDL_PumpEvents();
    clock++;
  }
  TTF_CloseFont(font);
  TTF_Quit();
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();
}

int main() {

  /*========================Variable declaration========================*/
  time_t t;
  srand((unsigned)time(&t));
  double QTable[NBPOS * NBETAT * NBAVGCOLUMN * NBTIC][3];
  int matrix[6] = {0, 0, 0, 0, 0, 0};
  double mat[(int)(pow(2, NBWIDTH) - 1)];
  int action[MAXTRY];
  int reward[MAXTRY];
  int state[MAXTRY];
  int iter = 0;
  int iterMax = 210000;
  int exploration = 100;
  float gamma = 0.99;
  float ksi = 0.2;
  int position;
  int displace;
  int etat = 0;
  int tabPosAsteroid[NBWIDTH] = {0};
  int died;
  int line0;       // This is to know when a line is to be empty or not
  int runTic;      // This is the tic that goes from 0 to MAXTRY
  int distanceTic; // This is the tic that goes from 0 to 3 that resets to 0
                   // every time the last asteroid line passes the ship It
                   // describes the amount of tics the ship has to escape the
                   // last line of asteroids

  /*========================Variable initialisation=====================*/
  initMat(mat);
  initMatrix(matrix, mat); // matrix is the state array (saves the states of
                           // each asteroid line)
  initQtable(QTable);

  
  /*====================== Updating variables ==========================*/
  while (iter < iterMax) {
    position = 2;
    died = 0;
    etat = 0;
    runTic = 0;
    distanceTic = 0;
    line0 = 1;
    int column;

    for (int i = 0; i < MAXTRY; i++) {
      action[i] = 0;
      reward[i] = 0;
      state[i] = 0;
    }
    runTic = 0;
    while (!died && runTic < MAXTRY) {
      if ((rand() % 101) <= exploration) {

        if ((runTic % 4) == 0) {
          distanceTic = 0;
          etat = computeNextEtat(mat, etat);
          if (!line0) {
            updateMatrix(matrix, etat);
            line0 = 1;
          } else {
            updateMatrix_0(matrix);
            line0 = 0;
          }
        } else {
          distanceTic++;
        }

        column = easyColumn(matrix);
        state[runTic] = matrix[5] * NBPOS * NBAVGCOLUMN * NBTIC +
                        position * NBAVGCOLUMN * NBTIC + column * NBTIC +
                        distanceTic;
        displace = rand() % 3;
        action[runTic] = displace;
        switch (displace) {
        case 0:
          if (position != 0) {
            position--;
          }
          break;
        case 2:
          if (position != 5) {
            position++;
          }
          break;
        }
        codeBin(matrix[5], tabPosAsteroid);
        if ((distanceTic == 3) && tabPosAsteroid[position]) {
          died = 1;
          reward[runTic] = -1; // runTic / NBTIC
        }
        runTic++;

      } else {
        if ((runTic % 4) == 0) {
          distanceTic = 0;
          etat = computeNextEtat(mat, etat);
          if (!line0) {
            updateMatrix(matrix, etat);
            line0 = 1;
          } else {
            updateMatrix_0(matrix);
            line0 = 0;
          }
        } else {
          distanceTic++;
        }

        column = easyColumn(matrix);
        state[runTic] = matrix[5] * NBPOS * NBAVGCOLUMN * NBTIC +
                        position * NBAVGCOLUMN * NBTIC + column * NBTIC +
                        distanceTic;
        /*Utilisation de la qtable*/
        float max = QTable[state[runTic]][0];
        int displace = 0;
        for (int i = 1; i < 3; i++) {
          if (QTable[state[runTic]][i] > max) {
            max = QTable[state[runTic]][i];
            displace = i;
          }
        }
        action[runTic] = displace;
        switch (displace) {
        case 0:
          if (position != 0) {
            position--;
          }
          break;
        case 2:
          if (position != 5) {
            position++;
          }
          break;
        }
        codeBin(matrix[5], tabPosAsteroid);
        if ((distanceTic == 3) && tabPosAsteroid[position]) {
          died = 1;
          reward[runTic] = -1; // runTic / NBTIC
        }
        runTic++;
      }
    }
    iter++;
    addScoreToFile(runTic,"scorevalues");
    if (iter > 50000) {
      if (iter % 1000 == 0) {
        if (exploration > 0) {
          exploration--;
        } else {
          exploration = 0;
        }
      }
    }

    updateQtable(QTable, state, action, reward, runTic, gamma, ksi);
    addQtableAvgToFile(QTable, "test");
  }

  exportQtable(QTable, "firstQtable");
  printf("Total learning iterations: %d\n", iter);

  /*======================Graphic part========================*/

  SDL_Window *window = NULL;
  SDL_Renderer *renderer = NULL;
  SDL_Texture *asteroid = NULL;
  SDL_Texture *character = NULL;
  SDL_Texture *menutxt = NULL;
  SDL_Texture *GOtxt = NULL; // Game Over overlay
  TTF_Font *font = NULL;     // la variable 'police de caractère'

  displayRun(window, renderer, asteroid, character, menutxt, GOtxt, font, QTable);
  return 0;
}
