#include "SDLmonitor.h"

/*===========================SDL End Function=================================*/
/*                          Given by documents                                */
void end_sdl(char ok, char const* msg, SDL_Window* window, SDL_Renderer* renderer){
  char msg_formated[255];
  int l;

  if (!ok){
    strncpy(msg_formated, msg, 250);
    l = strlen(msg_formated);
    strcpy(msg_formated + l, " : %s\n");

    SDL_Log(msg_formated, SDL_GetError());
  }

  if (renderer != NULL){
    SDL_DestroyRenderer(renderer);
    renderer = NULL;
  }
  if (window != NULL){
    SDL_DestroyWindow(window);
    window= NULL;
  }

  SDL_Quit();

  if (!ok){
    exit(EXIT_FAILURE);
  }
}
/*=======================SDL Initialization Function==========================*/
/*                          Given by documents                                */
void initSDL(){
  if (SDL_Init(SDL_INIT_VIDEO) < 0){
      SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Initialize SDL: %s",
                   SDL_GetError());
      exit(EXIT_FAILURE);
  }
}
