#include "play.h"
#include "asteroids.h"

/*========================Ship Position Function==============================*/
/*                    Set the position of the ship idle                       */
int Position(SDL_Rect window_dimensions, SDL_Rect destination, int pos, int offset){
    int x;
    int gap = window_dimensions.w/(int)(2*NBWIDTH) - destination.w/2;
    switch (pos){
        case 0:
            x = 0 * window_dimensions.w /NBWIDTH + gap + 10 * offset;
            break;
        case 1:
            x = 1 * window_dimensions.w /NBWIDTH + gap + 10 * offset;
            break;
        case 2:
            x = 2 * window_dimensions.w /NBWIDTH + gap + 10 * offset;
            break;
        case 3:
            x = 3 * window_dimensions.w /NBWIDTH + gap + 10 * offset;
            break;
        case 4:
            x = 4 * window_dimensions.w /NBWIDTH + gap + 10 * offset;
            break;
        case 5:
            x = 5 * window_dimensions.w /NBWIDTH + gap + 10 * offset;
            break;
    }
    return x;
}
