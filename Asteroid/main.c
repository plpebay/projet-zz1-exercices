#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <SDL2/SDL_ttf.h>

#include "background.h"
#include "spaceship.h"
#include "asteroids.h"


void end_sdl(char ok,                                               // fin normale : ok = 0 ; anormale ok = 1
             char const* msg,                                       // message à afficher
             SDL_Window* window,                                    // fenêtre à fermer
             SDL_Renderer* renderer) {                              // renderer à fermer
  char msg_formated[255];                                                         
  int l;                                                                          

  if (!ok) {                                                        // Affichage de ce qui ne va pas
    strncpy(msg_formated, msg, 250);                                              
    l = strlen(msg_formated);                                                     
    strcpy(msg_formated + l, " : %s\n");                                          

    SDL_Log(msg_formated, SDL_GetError());                                        
  }                                                                               

  if (renderer != NULL) {                                           // Destruction si nécessaire du renderer
    SDL_DestroyRenderer(renderer);                                  // Attention : on suppose que les NULL sont maintenus !!
    renderer = NULL;
  }
  if (window != NULL)   {                                           // Destruction si nécessaire de la fenêtre
    SDL_DestroyWindow(window);                                      // Attention : on suppose que les NULL sont maintenus !!
    window= NULL;
  }

  SDL_Quit();                                                                     

  if (!ok) {                                                        // On quitte si cela ne va pas                
    exit(EXIT_FAILURE);                                                           
  }                                                                               
}

void initSDL() {
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
      SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Initialize SDL: %s",
                   SDL_GetError());
      exit(EXIT_FAILURE);
  }
}

void Text(SDL_Window *window, SDL_Renderer *renderer, TTF_Font* font, char *word)
{
  
  SDL_Color color = {0, 0, 0, 255};// la couleur du texte
  SDL_Surface* text_surface = NULL;// la surface  (uniquement transitoire)

  text_surface = TTF_RenderText_Blended(font, word, color); // création du texte dans la surface 
  if (text_surface == NULL) end_sdl(0, "Can't create text surface", window, renderer);

  SDL_Texture* text_texture = NULL;// la texture qui contient le texte
  text_texture = SDL_CreateTextureFromSurface(renderer, text_surface); // transfert de la surface à la texture
  if (text_texture == NULL) end_sdl(0, "Can't create texture from surface", window, renderer);
  SDL_FreeSurface(text_surface); 

  

  //
  SDL_Rect pos = {0, 0, 0, 0};// rectangle où le texte va être positionné
  SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);// récupération de la taille (w, h) du texte 
  SDL_RenderCopy(renderer, text_texture, NULL, &pos);// Ecriture du texte dans le renderer   
  //

} 
 
int main(){
  int etat = 0;
  int matrix[6] = {0, 0, 0, 0, 0, 0};
  int animationOffset;
  int offset = 10; //nb de pixels pour decaler pour rendre l'animation fluide

  int mat[7][7] = {{10, 10, 10, 20, 10, 20, 20},
                  {10, 15, 10, 25, 15, 25, 0},
                  {10, 70, 10, 0, 0, 0, 0},
                  {10, 30, 30, 30, 0, 0, 0},
                  {10, 35, 0, 0, 25, 15, 15},
                  {10, 35, 0, 0, 35, 20, 0},
                  {10, 0, 15, 0, 60, 0, 15}};
    initSDL();
    time_t t;
    srand((unsigned) time(&t));

    /*window*/
    SDL_Window *window = NULL;
    SDL_Renderer *renderer;


    if (TTF_Init() < 0) end_sdl(0, "Couldn't initialize SDL TTF", window, renderer);
    
    
    if (SDL_CreateWindowAndRenderer(500, 800, 0, &window,
                                    &renderer) < 0) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
                     "Create window and renderer: %s", SDL_GetError());
        return EXIT_FAILURE;
    }

    SDL_Texture *asteroid;
    asteroid = IMG_LoadTexture(renderer,"asteroid.png");
    if (asteroid == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", window, renderer);
    SDL_SetWindowTitle(window, "Asteroid");


    /*character*/
    SDL_Texture * character = NULL;
    character = IMG_LoadTexture(renderer,"ship.png");
    if (character == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", window, renderer);
    
    SDL_Rect
            window_dimensions = {0},
            source = {0},
            state = {0},
            destination = {0}; 

    SDL_GetWindowSize(window, &window_dimensions.w,&window_dimensions.h);
    SDL_QueryTexture(character, NULL, NULL, &source.w, &source.h);

    int nb_images = 4;
    float zoom = 0.3;
    int offset_x = source.w / nb_images,
        offset_y = source.h;
    
    state.x = 0;
    state.y = 0;
    state.w = offset_x;
    state.h = offset_y;

    destination.w = offset_x * zoom;
    destination.h = offset_y * zoom;

    destination.x = (window_dimensions.w - destination.w)/2;
    destination.y = 9*(window_dimensions.h - destination.h )/10 + 30;

    int maxX = window_dimensions.w - destination.w;


    /*menu*/
    SDL_Texture * menutxt = NULL;
    menutxt = IMG_LoadTexture(renderer,"menu.png");
    if (menutxt == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", window, renderer);

    SDL_Rect
            sourcetxt = {0},
            destinationtxt = {0};
    
    SDL_QueryTexture(menutxt, NULL, NULL, &sourcetxt.w, &sourcetxt.h);

    destinationtxt.x = window_dimensions.x + 75;
    destinationtxt.y = window_dimensions.y + 200;
    destinationtxt.w = sourcetxt.w * 0.6;
    destinationtxt.h = sourcetxt.h * 0.6;


    /*Score*/
    TTF_Font* font = NULL; // la variable 'police de caractère'
    font = TTF_OpenFont("Space-font.ttf", 25);// La police à charger, la taille désirée
    if (font == NULL) end_sdl(0, "Can't load font", window, renderer);

    TTF_SetFontStyle(font, TTF_STYLE_ITALIC | TTF_STYLE_BOLD);

    char scored[18] = "Score:";
    char initscore[7] = "Score:";
    int inc = 1;
    char strinc[10];

    /*menu*/
    SDL_Texture * GOtxt = NULL;
    GOtxt = IMG_LoadTexture(renderer,"GameOver.png");
    if (GOtxt == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", window, renderer);

    SDL_Rect
            sourceGO = {0},
            destinationGO = {0};
    
    SDL_QueryTexture(GOtxt, NULL, NULL, &sourceGO.w, &sourceGO.h);

    destinationGO.x = window_dimensions.x + 50;
    destinationGO.y = window_dimensions.y + 300;
    destinationGO.w = sourceGO.w * 0.5;
    destinationGO.h = sourceGO.h * 0.5;




    SDL_bool quit = SDL_FALSE;
    //SDL_bool mouse_hover = SDL_FALSE;
    SDL_bool paused = SDL_TRUE;
    SDL_bool GameOver = SDL_FALSE;
    int delay = 100;
    int cpt = 0;

    while (!quit) {
        SDL_Event event;
        //SDL_bool trouve = SDL_FALSE;
        while ( SDL_PollEvent(&event)) { // gestion clavier souris
          switch (event.type) {
            case SDL_KEYDOWN:
              switch(event.key.keysym.sym){
                case SDLK_ESCAPE:
                  quit = SDL_TRUE;
                  break;
                case SDLK_SPACE:
                  paused = !paused;
                  break;
                /*Left*/
                case SDLK_q:
                case SDLK_LEFT:
                  if ( !paused && destination.x >= 0)
                  {
                    destination.x -= 10;
                  }
                  break;
                /*Right*/
                case SDLK_d:
                case SDLK_RIGHT:
                  if ( !paused && destination.x <= maxX )
                  {
                    destination.x += 10;
                  }
                  break;
                default:
                  break;
              }
            break;
            case SDL_QUIT:
                quit = SDL_TRUE;
                //trouve = SDL_TRUE;
                break;
          }
        }
        


        

        SDL_RenderClear(renderer);

        drawStars(renderer, window);
        SDL_RenderCopy(renderer, character, &state, &destination);
        state.x += offset_x;
        state.x %= source.w;

        score(renderer,window);

        if ( GameOver )
        {
          GOmenu(renderer,window);
          SDL_RenderCopy(renderer, GOtxt, &sourceGO, &destinationGO);

          sprintf(strinc, "%d", inc);
          strcat(scored,strinc);
          Text(window, renderer, font, scored);
          memset(scored, 0, strlen(scored));
          strcat(scored,initscore);

          if ( paused )
          {
            for ( int i = 0 ; i <= 5 ; i++ )
            {
              matrix[i] = 0;
            }
            inc = 0;
            GameOver = !GameOver;
            destination.x = (window_dimensions.w - destination.w)/2;
          }
        }
        else
        { 
          if ( paused )
          {
            menu(renderer,window);
            Text(window, renderer, font, scored);
            
            SDL_RenderCopy(renderer, menutxt, &sourcetxt, &destinationtxt);
            
          }
          else
          {
            sprintf(strinc, "%d", inc);
            strcat(scored,strinc);
            Text(window, renderer, font, scored);

            memset(scored, 0, strlen(scored));
            strcat(scored,initscore);
            inc++;

            
            animationOffset += offset -1;
            if(cpt % ((800 / 6) / offset) == 0)
            {
              animationOffset = 0;
              etat = computeNextEtat(mat, etat);
              updateAsteroids(matrix, etat);
            }
            cpt++;
            for (int j = 0; j < 6; j++){              // on parcours la matrice contenant les etats
              GameOver = showAsteroids(matrix, asteroid, window, renderer, animationOffset, j, destination.x, quit);
            }
          }
        }         
        

        
        SDL_RenderPresent(renderer);
        SDL_Delay(delay);
        SDL_PumpEvents();

    }

    //
    //SDL_FreeSurface(text_surface);
    TTF_CloseFont(font);
    TTF_Quit();
    //

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return EXIT_SUCCESS;
}
