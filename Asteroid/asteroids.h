#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int computeNextEtat(int m[7][7], int etat);
void updateAsteroids(int matrix[6], int etat);

SDL_bool showAsteroids(int matrix[6],
                           SDL_Texture *my_texture,
                           SDL_Window *window,
                           SDL_Renderer *renderer,
                           int animationOffset,
                         int j,
                         int dest,SDL_bool quit);
