#include "spaceship.h"
#include <SDL2/SDL_ttf.h>

void menu(SDL_Renderer* renderer, SDL_Window* window)
{

    SDL_Rect window_dimensions = {0};

    SDL_GetWindowSize(window, &window_dimensions.w,&window_dimensions.h);

    SDL_Rect menu;

    menu.x = window_dimensions.w/10;
    menu.y = window_dimensions.h/10;
    menu.w = 8*window_dimensions.w/10;
    menu.h = 6*window_dimensions.h/10;

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 150);
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

    SDL_RenderFillRect(renderer, &menu);

}

void score(SDL_Renderer* renderer, SDL_Window* window)
{

    SDL_Rect window_dimensions = {0};

    SDL_GetWindowSize(window, &window_dimensions.w,&window_dimensions.h);

    SDL_Rect score;

    score.x = 0;
    score.y = 0;
    score.w = window_dimensions.w;
    score.h = window_dimensions.h/30;

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 200);
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

    SDL_RenderFillRect(renderer, &score);

}


void GOmenu(SDL_Renderer* renderer, SDL_Window* window)
{

    SDL_Rect window_dimensions = {0};

    SDL_GetWindowSize(window, &window_dimensions.w,&window_dimensions.h);

    SDL_Rect menu;

    menu.x = 0;
    menu.y = 0;
    menu.w = window_dimensions.w;
    menu.h = window_dimensions.h;

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

    SDL_RenderFillRect(renderer, &menu);
}