#include "asteroids.h"

int computeNextEtat(int m[7][7], int etat){
  int val = rand() % 101;
  int cumul = 0;
  int i = 0;
  while(val >= cumul && i < 7){
    cumul += m[etat][i];
    i++;
  }
  if (i != 0){  
    etat = i - 1;
  }
  else{
    etat = 0;
  }
  return etat;
}

void updateAsteroids(int matrix[6], int etat){
  for(int i = 4; i > -1; i--){
    matrix[i+1] = matrix[i];
  }
  matrix[0] = etat;
}

SDL_bool showAsteroids(int matrix[6],
                           SDL_Texture *my_texture,
                           SDL_Window *window,
                           SDL_Renderer *renderer,
                           int animationOffset,
                         int j,
                         int dest,SDL_bool quit) {
  int width_ship = 70;
  SDL_Rect
    source = {0},                             // Rectangle définissant la zone de la texture à récupérer
    window_dimensions = {0},                  // Rectangle définissant la fenêtre, on  n'utilisera que largeur et hauteur
    destination = {-200};                        // Rectangle définissant où la zone_source doit être déposée dans le renderer
    SDL_Rect
      source2 = {0},
      //window_dimensions2 = {0},
      destination2 = {-200};

  SDL_GetWindowSize(window,                   // Récupération des dimensions de la fenêtre
                    &window_dimensions.w,
                    &window_dimensions.h);
  SDL_QueryTexture(my_texture, NULL, NULL,    // Récupération des dimensions de l'image
                   &source.w, &source.h);
  SDL_QueryTexture(my_texture, NULL, NULL,    // Récupération des dimensions de l'image
                  &source2.w, &source2.h);


  float zoom = 0.3;                             // zoom, car ces images sont un peu petites
  int offset_x_1 = source.w,                // La largeur d'une vignette de l'image
      offset_y_1 = source.h;                // La hauteur d'une vignette de l'image
  int offset_x_2 = source2.w,                // La largeur d'une vignette de l'image
      offset_y_2 = source2.h;                // La hauteur d'une vignette de l'image


  destination.w = offset_x_1 * zoom;            // Largeur du sprite à l'écran
  destination.h = offset_y_1 * zoom;            // Hauteur du sprite à l'écran
  destination2.w = offset_x_2 * zoom;            // Largeur du sprite à l'écran
  destination2.h = offset_y_2 * zoom;            // Hauteur du sprite à l'écran
  int width_column = window_dimensions.w / 3;
  int height_column = window_dimensions.h / 7;

  /* La fonction que je fais */

    switch(matrix[j]){

      case 1:
      destination.x = width_column * 2 + 20; // Position en x pour l'affichage du sprite
      destination.y = height_column * j + animationOffset;  // Position en y pour l'affichage du sprite
      break;
      case 2:
      destination.x = width_column * 1 + 20; // Position en x pour l'affichage du sprite
      destination.y = height_column * j + animationOffset;  // Position en y pour l'affichage du sprite
      break;
      case 3:
      destination.x = width_column * 1 + 20; // Position en x pour l'affichage du sprite
      destination.y = height_column * j + animationOffset;  // Position en y pour l'affichage du sprite
      destination2.x = width_column * 2 + 20; // Position en x pour l'affichage du sprite
      destination2.y = height_column * j + animationOffset;  // Position en y pour l'affichage du sprite
      break;
      case 4:
      destination.x = width_column * 0 + 20; // Position en x pour l'affichage du sprite
      destination.y = height_column * j + animationOffset;  // Position en y pour l'affichage du sprite
      break;
      case 5:
      destination.x = width_column * 0 + 20; // Position en x pour l'affichage du sprite
      destination.y = height_column * j + animationOffset;  // Position en y pour l'affichage du sprite
      destination2.x = width_column * 2 + 20; // Position en x pour l'affichage du sprite
      destination2.y = height_column * j + animationOffset;  // Position en y pour l'affichage du sprite
      break;
      case 6:
      destination.x = width_column * 0 + 20; // Position en x pour l'affichage du sprite
      destination.y = height_column * j + animationOffset;  // Position en y pour l'affichage du sprite
      destination2.x = width_column * 1 + 20; // Position en x pour l'affichage du sprite
      destination2.y = height_column * j + animationOffset;  // Position en y pour l'affichage du sprite
      break;
      default:
      break;

}

if( (dest>=destination.x && dest<=destination.x+destination.w && destination.y>590) ||
  (dest>=destination2.x && dest<=destination2.x+destination2.w && destination2.y>590) ||
  (dest + width_ship >=destination.x && dest + width_ship <=destination.x+destination.w && destination.y>590) ||
  (dest + width_ship>=destination2.x && dest + width_ship<=destination2.x+destination2.w && destination2.y>590)){
  quit = SDL_TRUE;
}
  if(matrix[j] != 0){
    SDL_RenderCopy(renderer,                  // Préparation de l'affichage
                     my_texture, &source, &destination);
    SDL_RenderCopy(renderer,                  // Préparation de l'affichage
                    my_texture, &source2, &destination2);
  }
  return quit;
}
