#include "background.h"

void drawStars(SDL_Renderer* renderer, SDL_Window* window) {
  int size_point = 5;
  

  SDL_Rect window_dimensions = {0};
  SDL_Rect rectangle;

  SDL_GetWindowSize(
      window, &window_dimensions.w,
      &window_dimensions.h);
  rectangle.x = 0;
  rectangle.y = 0;
  rectangle.w = window_dimensions.w;
  rectangle.h = window_dimensions.h;

  SDL_SetRenderDrawColor(renderer,
                         15, 10, 40,                                  // mode Red, Green, Blue (tous dans 0..255)
                         255);                                       // sa hauteur (h = height)

  SDL_RenderFillRect(renderer, &rectangle);                                // x,y seconde extrémité

SDL_SetRenderDrawColor(renderer,                                    // couleur des points aléatoire mais la même pour
                         255, 255, 255,  // chaque point.
                         180);                                      // opacité = opaque

  /* Traçage de points aléatoire à l'intérieur d'un disque */
  for (int i = 0; i < 40 ; i++) {
    int rand_1 = rand() % window_dimensions.w-size_point;
    int rand_2 = rand() % window_dimensions.h-size_point;
  for (int j=0; j<size_point; j++){
    for (int k=0; k<size_point; k++){
    SDL_RenderDrawPoint(renderer,
                         rand_1 + j,                     // coordonnée en x
                         rand_2 + k);                    //            en y
    }
  }
  }
}


